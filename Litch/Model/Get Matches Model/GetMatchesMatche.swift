//
//	GetMatchesMatche.swift
//
//	Create by mac on 24/7/2020
//	Copyright © 2020. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GetMatchesMatche : NSObject, NSCoding{

	var matchId : String!
	var person : GetMatchesPerson!
    var lastChatMessage : String! = ""

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		matchId = dictionary["matchId"] as? String
        lastChatMessage = dictionary["lastChat"] as? String
		if let personData = dictionary["person"] as? [String:Any]{
			person = GetMatchesPerson(fromDictionary: personData)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if matchId != nil{
			dictionary["matchId"] = matchId
		}
		if person != nil{
			dictionary["person"] = person.toDictionary()
		}
        if lastChatMessage != nil {
            dictionary["lastChat"] = lastChatMessage
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         matchId = aDecoder.decodeObject(forKey: "matchId") as? String
         person = aDecoder.decodeObject(forKey: "person") as? GetMatchesPerson
        lastChatMessage = aDecoder.decodeObject(forKey: "lastChat") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if matchId != nil{
			aCoder.encode(matchId, forKey: "matchId")
		}
		if person != nil{
			aCoder.encode(person, forKey: "person")
		}
        if lastChatMessage != nil {
            aCoder.encode(lastChatMessage, forKey: "lastChat")
        }

	}

}
