//
//	GetMatchesModel.swift
//
//	Create by mac on 24/7/2020
//	Copyright © 2020. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GetMatchesModel : NSObject, NSCoding{

	var matches : [GetMatchesMatche]!
	var message : String!
	var status : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		matches = [GetMatchesMatche]()
		if let matchesArray = dictionary["matches"] as? [[String:Any]]{
			for dic in matchesArray{
				let value = GetMatchesMatche(fromDictionary: dic)
				matches.append(value)
			}
		}
		message = dictionary["message"] as? String
		status = dictionary["status"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if matches != nil{
			var dictionaryElements = [[String:Any]]()
			for matchesElement in matches {
				dictionaryElements.append(matchesElement.toDictionary())
			}
			dictionary["matches"] = dictionaryElements
		}
		if message != nil{
			dictionary["message"] = message
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         matches = aDecoder.decodeObject(forKey :"matches") as? [GetMatchesMatche]
         message = aDecoder.decodeObject(forKey: "message") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if matches != nil{
			aCoder.encode(matches, forKey: "matches")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}