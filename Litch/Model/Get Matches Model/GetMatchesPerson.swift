//
//	GetMatchesPerson.swift
//
//	Create by mac on 24/7/2020
//	Copyright © 2020. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GetMatchesPerson : NSObject, NSCoding{

	var id : String!
	var email : String!
	var litch : String!
	var litchImage : String!
	var name : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		id = dictionary["_id"] as? String
		email = dictionary["email"] as? String
		litch = dictionary["litch"] as? String
		litchImage = dictionary["litchImage"] as? String
		name = dictionary["name"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if id != nil{
			dictionary["_id"] = id
		}
		if email != nil{
			dictionary["email"] = email
		}
		if litch != nil{
			dictionary["litch"] = litch
		}
		if litchImage != nil{
			dictionary["litchImage"] = litchImage
		}
		if name != nil{
			dictionary["name"] = name
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "_id") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         litch = aDecoder.decodeObject(forKey: "litch") as? String
         litchImage = aDecoder.decodeObject(forKey: "litchImage") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "_id")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if litch != nil{
			aCoder.encode(litch, forKey: "litch")
		}
		if litchImage != nil{
			aCoder.encode(litchImage, forKey: "litchImage")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}

	}

}