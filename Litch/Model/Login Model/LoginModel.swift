//
//	LoginModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class LoginModel : NSObject, NSCoding{
    
	var message : String!
	var name : String!
	var status : Int!
	var token : String!
    var questions : [String]!
    var maximumLitchTime : String!

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
     init(fromDictionary dictionary: [String:Any]){
        
        message = dictionary["message"] as? String
        name = dictionary["name"] as? String
        status = dictionary["status"] as? Int
        token = dictionary["token"] as? String
        questions = dictionary["questions"] as? [String]
        maximumLitchTime = dictionary["maximumLitchTime"] as? String
    }

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if message != nil{
			dictionary["message"] = message
		}
		if name != nil{
			dictionary["name"] = name
		}
		if status != nil{
			dictionary["status"] = status
		}
		if token != nil{
			dictionary["token"] = token
		}
        if questions != nil{
            dictionary["questions"] = questions
        }
        if maximumLitchTime != nil {
            dictionary["maximumLitchTime"] = maximumLitchTime
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         message = aDecoder.decodeObject(forKey: "message") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Int
         token = aDecoder.decodeObject(forKey: "token") as? String
        questions = aDecoder.decodeObject(forKey: "questions") as? [String]
        maximumLitchTime = aDecoder.decodeObject(forKey: "maximumLitchTime") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if token != nil{
			aCoder.encode(token, forKey: "token")
		}
        if questions != nil{
            aCoder.encode(questions, forKey: "questions")
        }
        if maximumLitchTime != nil {
            aCoder.encode(maximumLitchTime, forKey: "maximumLitchTime")
        }
	}
    
    func saveInUserDefault(){
        let userDefaults = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: self)
        userDefaults.set(encodedData, forKey:Constant.UserDefaultKeys.currentUserModel)
        userDefaults.synchronize()
    }
}
