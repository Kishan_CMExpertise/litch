//
//	LikeModel.swift
//
//	Create by mac on 20/7/2020
//	Copyright © 2020. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class LikeModel : NSObject, NSCoding{

	var likedUsersEmail : String!
	var likedUsersName : String!
	var message : String!
	var status : Int!
    var match : Bool!

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		likedUsersEmail = dictionary["likedUsersEmail"] as? String
		likedUsersName = dictionary["likedUsersName"] as? String
		message = dictionary["message"] as? String
		status = dictionary["status"] as? Int
        match = dictionary["match"] as? Bool
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if likedUsersEmail != nil{
			dictionary["likedUsersEmail"] = likedUsersEmail
		}
		if likedUsersName != nil{
			dictionary["likedUsersName"] = likedUsersName
		}
		if message != nil{
			dictionary["message"] = message
		}
		if status != nil{
			dictionary["status"] = status
		}
        if match != nil{
            dictionary["match"] = match
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         likedUsersEmail = aDecoder.decodeObject(forKey: "likedUsersEmail") as? String
         likedUsersName = aDecoder.decodeObject(forKey: "likedUsersName") as? String
         message = aDecoder.decodeObject(forKey: "message") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Int
        match = aDecoder.decodeObject(forKey: "match") as? Bool
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if likedUsersEmail != nil{
			aCoder.encode(likedUsersEmail, forKey: "likedUsersEmail")
		}
		if likedUsersName != nil{
			aCoder.encode(likedUsersName, forKey: "likedUsersName")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
        if match != nil{
            aCoder.encode(match, forKey: "match")
        }

	}

}
