//
//	GetLikesReceivedLike.swift
//
//	Create by mac on 29/7/2020
//	Copyright © 2020. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GetLikesReceivedLike : NSObject, NSCoding{

	var v : Int!
	var id : String!
	var age : Int!
	var litch : String!
	var litchImage : String!
	var location : String!
	var name : String!
	var userId : String!
    var litchQuestion : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		v = dictionary["__v"] as? Int
		id = dictionary["_id"] as? String
		age = dictionary["age"] as? Int
		litch = dictionary["litch"] as? String
		litchImage = dictionary["litchImage"] as? String
		location = dictionary["location"] as? String
		name = dictionary["name"] as? String
		userId = dictionary["userId"] as? String
        litchQuestion = dictionary["litchQuestion"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if v != nil{
			dictionary["__v"] = v
		}
		if id != nil{
			dictionary["_id"] = id
		}
		if age != nil{
			dictionary["age"] = age
		}
		if litch != nil{
			dictionary["litch"] = litch
		}
		if litchImage != nil{
			dictionary["litchImage"] = litchImage
		}
		if location != nil{
			dictionary["location"] = location
		}
		if name != nil{
			dictionary["name"] = name
		}
		if userId != nil{
			dictionary["userId"] = userId
		}
        if litchQuestion != nil {
            dictionary["litchQuestion"] = litchQuestion
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         v = aDecoder.decodeObject(forKey: "__v") as? Int
         id = aDecoder.decodeObject(forKey: "_id") as? String
         age = aDecoder.decodeObject(forKey: "age") as? Int
         litch = aDecoder.decodeObject(forKey: "litch") as? String
         litchImage = aDecoder.decodeObject(forKey: "litchImage") as? String
         location = aDecoder.decodeObject(forKey: "location") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         userId = aDecoder.decodeObject(forKey: "userId") as? String
        litchQuestion = aDecoder.decodeObject(forKey: "litchQuestion") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if v != nil{
			aCoder.encode(v, forKey: "__v")
		}
		if id != nil{
			aCoder.encode(id, forKey: "_id")
		}
		if age != nil{
			aCoder.encode(age, forKey: "age")
		}
		if litch != nil{
			aCoder.encode(litch, forKey: "litch")
		}
		if litchImage != nil{
			aCoder.encode(litchImage, forKey: "litchImage")
		}
		if location != nil{
			aCoder.encode(location, forKey: "location")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "userId")
		}
        if litchQuestion != nil  {
            aCoder.encode(litchQuestion, forKey: "litchQuestion")
        }
	}
}
