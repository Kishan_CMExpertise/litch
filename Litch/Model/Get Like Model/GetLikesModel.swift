//
//	GetLikesModel.swift
//
//	Create by mac on 29/7/2020
//	Copyright © 2020. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GetLikesModel : NSObject, NSCoding{

	var message : String!
	var receivedLikes : [GetLikesReceivedLike]!
	var status : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		message = dictionary["message"] as? String
		receivedLikes = [GetLikesReceivedLike]()
		if let receivedLikesArray = dictionary["receivedLikes"] as? [[String:Any]]{
			for dic in receivedLikesArray{
				let value = GetLikesReceivedLike(fromDictionary: dic)
				receivedLikes.append(value)
			}
		}
		status = dictionary["status"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if message != nil{
			dictionary["message"] = message
		}
		if receivedLikes != nil{
			var dictionaryElements = [[String:Any]]()
			for receivedLikesElement in receivedLikes {
				dictionaryElements.append(receivedLikesElement.toDictionary())
			}
			dictionary["receivedLikes"] = dictionaryElements
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         message = aDecoder.decodeObject(forKey: "message") as? String
         receivedLikes = aDecoder.decodeObject(forKey :"receivedLikes") as? [GetLikesReceivedLike]
         status = aDecoder.decodeObject(forKey: "status") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if receivedLikes != nil{
			aCoder.encode(receivedLikes, forKey: "receivedLikes")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}