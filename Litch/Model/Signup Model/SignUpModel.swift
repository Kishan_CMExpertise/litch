//
//	SignUpModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SignUpModel : NSObject, NSCoding{

	var email : String!
	var message : String!
	var questions : [String]!
	var status : Int!
	var token : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		email = dictionary["email"] as? String
		message = dictionary["message"] as? String
		questions = dictionary["questions"] as? [String]
		status = dictionary["status"] as? Int
		token = dictionary["token"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if email != nil{
			dictionary["email"] = email
		}
		if message != nil{
			dictionary["message"] = message
		}
		if questions != nil{
			dictionary["questions"] = questions
		}
		if status != nil{
			dictionary["status"] = status
		}
		if token != nil{
			dictionary["token"] = token
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         email = aDecoder.decodeObject(forKey: "email") as? String
         message = aDecoder.decodeObject(forKey: "message") as? String
         questions = aDecoder.decodeObject(forKey: "questions") as? [String]
         status = aDecoder.decodeObject(forKey: "status") as? Int
         token = aDecoder.decodeObject(forKey: "token") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if questions != nil{
			aCoder.encode(questions, forKey: "questions")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if token != nil{
			aCoder.encode(token, forKey: "token")
		}

	}

}