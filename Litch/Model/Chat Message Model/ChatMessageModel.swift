//
//	ChatMessageModel.swift
//
//	Create by mac on 24/7/2020
//	Copyright © 2020. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ChatMessageModel : NSObject, NSCoding{

	var chatHistory : [ChatMessageChatHistory]!
	var message : String!
	var status : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		chatHistory = [ChatMessageChatHistory]()
		if let chatHistoryArray = dictionary["chatHistory"] as? [[String:Any]]{
			for dic in chatHistoryArray{
				let value = ChatMessageChatHistory(fromDictionary: dic)
				chatHistory.append(value)
			}
		}
		message = dictionary["message"] as? String
		status = dictionary["status"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if chatHistory != nil{
			var dictionaryElements = [[String:Any]]()
			for chatHistoryElement in chatHistory {
				dictionaryElements.append(chatHistoryElement.toDictionary())
			}
			dictionary["chatHistory"] = dictionaryElements
		}
		if message != nil{
			dictionary["message"] = message
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         chatHistory = aDecoder.decodeObject(forKey :"chatHistory") as? [ChatMessageChatHistory]
         message = aDecoder.decodeObject(forKey: "message") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if chatHistory != nil{
			aCoder.encode(chatHistory, forKey: "chatHistory")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}