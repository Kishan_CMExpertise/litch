//
//	ChatMessageChatHistory.swift
//
//	Create by mac on 24/7/2020
//	Copyright © 2020. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ChatMessageChatHistory : NSObject, NSCoding{

	var v : Int!
	var id : String!
	var author : ChatMessageAuthor!
	var text : String!
	var time : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		v = dictionary["__v"] as? Int
		id = dictionary["_id"] as? String
		if let authorData = dictionary["author"] as? [String:Any]{
			author = ChatMessageAuthor(fromDictionary: authorData)
		}
		text = dictionary["text"] as? String
		time = dictionary["time"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if v != nil{
			dictionary["__v"] = v
		}
		if id != nil{
			dictionary["_id"] = id
		}
		if author != nil{
			dictionary["author"] = author.toDictionary()
		}
		if text != nil{
			dictionary["text"] = text
		}
		if time != nil{
			dictionary["time"] = time
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         v = aDecoder.decodeObject(forKey: "__v") as? Int
         id = aDecoder.decodeObject(forKey: "_id") as? String
         author = aDecoder.decodeObject(forKey: "author") as? ChatMessageAuthor
         text = aDecoder.decodeObject(forKey: "text") as? String
         time = aDecoder.decodeObject(forKey: "time") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if v != nil{
			aCoder.encode(v, forKey: "__v")
		}
		if id != nil{
			aCoder.encode(id, forKey: "_id")
		}
		if author != nil{
			aCoder.encode(author, forKey: "author")
		}
		if text != nil{
			aCoder.encode(text, forKey: "text")
		}
		if time != nil{
			aCoder.encode(time, forKey: "time")
		}

	}

}