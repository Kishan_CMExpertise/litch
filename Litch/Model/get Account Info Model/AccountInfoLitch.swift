//
//	AccountInfoLitch.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class AccountInfoLitch : NSObject, NSCoding{
    
	var v : Int!
	var id : String!
	var author : AccountInfoAuthor!
	var imageFile : String!
	var question : String!
	var time : String!
	var videoFile : String!

    /**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
    init(fromDictionary dictionary: [String:Any]){
        v = dictionary["__v"] as? Int
        id = dictionary["_id"] as? String
        if let authorData = dictionary["author"] as? [String:Any]{
            author = AccountInfoAuthor(fromDictionary: authorData)
        }
        imageFile = dictionary["imageFile"] as? String
        question = dictionary["question"] as? String
        time = dictionary["time"] as? String
        videoFile = dictionary["videoFile"] as? String
    }

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if v != nil{
			dictionary["__v"] = v
		}
		if id != nil{
			dictionary["_id"] = id
		}
		if author != nil{
			dictionary["author"] = author.toDictionary()
		}
		if imageFile != nil{
			dictionary["imageFile"] = imageFile
		}
		if question != nil{
			dictionary["question"] = question
		}
		if time != nil{
			dictionary["time"] = time
		}
		if videoFile != nil{
			dictionary["videoFile"] = videoFile
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         v = aDecoder.decodeObject(forKey: "__v") as? Int
         id = aDecoder.decodeObject(forKey: "_id") as? String
         author = aDecoder.decodeObject(forKey: "author") as? AccountInfoAuthor
         imageFile = aDecoder.decodeObject(forKey: "imageFile") as? String
         question = aDecoder.decodeObject(forKey: "question") as? String
         time = aDecoder.decodeObject(forKey: "time") as? String
         videoFile = aDecoder.decodeObject(forKey: "videoFile") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if v != nil{
			aCoder.encode(v, forKey: "__v")
		}
		if id != nil{
			aCoder.encode(id, forKey: "_id")
		}
		if author != nil{
			aCoder.encode(author, forKey: "author")
		}
		if imageFile != nil{
			aCoder.encode(imageFile, forKey: "imageFile")
		}
		if question != nil{
			aCoder.encode(question, forKey: "question")
		}
		if time != nil{
			aCoder.encode(time, forKey: "time")
		}
		if videoFile != nil{
			aCoder.encode(videoFile, forKey: "videoFile")
		}
	}
}
