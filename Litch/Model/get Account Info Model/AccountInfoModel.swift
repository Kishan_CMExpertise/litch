//
//	AccountInfoModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class AccountInfoModel : NSObject, NSCoding{

//    static let sharedInstance = AccountInfoModel()
    
	var age : Int!
	var litch : AccountInfoLitch!
	var location : String!
	var message : String!
	var name : String!
	var status : Int!


    func setLoginResponse(fromDictionary dictionary: [String:Any]){
        age = dictionary["age"] as? Int
                if let litchData = dictionary["litch"] as? [String:Any]{
                    litch = AccountInfoLitch(fromDictionary: litchData)
//                    AccountInfoLitch.sharedInstance.setLoginResponse(fromDictionary: litchData)
                }
                location = dictionary["location"] as? String
                message = dictionary["message"] as? String
                name = dictionary["name"] as? String
                status = dictionary["status"] as? Int
    }
	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		age = dictionary["age"] as? Int
		if let litchData = dictionary["litch"] as? [String:Any]{
			litch = AccountInfoLitch(fromDictionary: litchData)
//            AccountInfoLitch.sharedInstance.setLoginResponse(fromDictionary: litchData)
		}
		location = dictionary["location"] as? String
		message = dictionary["message"] as? String
		name = dictionary["name"] as? String
		status = dictionary["status"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if age != nil{
			dictionary["age"] = age
		}
		if litch != nil{
			dictionary["litch"] = litch.toDictionary()
		}
		if location != nil{
			dictionary["location"] = location
		}
		if message != nil{
			dictionary["message"] = message
		}
		if name != nil{
			dictionary["name"] = name
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
//        self.init()
        age = aDecoder.decodeObject(forKey: "age") as? Int
        litch = aDecoder.decodeObject(forKey: "litch") as? AccountInfoLitch
        location = aDecoder.decodeObject(forKey: "location") as? String
        message = aDecoder.decodeObject(forKey: "message") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        status = aDecoder.decodeObject(forKey: "status") as? Int
        
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if age != nil{
			aCoder.encode(age, forKey: "age")
		}
		if litch != nil{
			aCoder.encode(litch, forKey: "litch")
		}
		if location != nil{
			aCoder.encode(location, forKey: "location")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
let userInfoKey: String = "AccountInfoModel"

class UserInfoManager: NSObject {
    
    //MARK:- Setter Method
    class func setUserInfo(userInfoModel: AccountInfoModel!) {
        if let userModel = userInfoModel {
            let archivedServerModules = NSKeyedArchiver.archivedData(withRootObject: userModel)
            UserDefaults.standard.setValue(archivedServerModules, forKey: userInfoKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    //MARK:- Getter Method
    class func getUserInfoModel() -> AccountInfoModel! {
        let archivedServerModules: AnyObject? = UserDefaults.standard.value(forKey: userInfoKey) as AnyObject?
        
        if archivedServerModules == nil {
            return nil
        }
        if let data = archivedServerModules as? NSData {
            if let userInfoModel = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? AccountInfoModel {
                return userInfoModel
            } }
        return nil
    }
    
    //MARK:- Remove user's info
    class func removeUserInfo() {
        UserDefaults.standard.removeObject(forKey: userInfoKey)
        UserDefaults.standard.synchronize()
    }
}
