//
//  CustomView.swift
//  TinderSwipeView_Example
//
//  Created by Nick on 29/05/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

struct UserModel {

    let name : String!
    let num : String!
    let videoUrl : String!
    let imgUrl : String!
    let question : String!
}


class CustomView: UIView {
        
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imageViewBackground: UIImageView!
    @IBOutlet weak var btnReply: UIButton!
    
    var playerUrl: URL!
    var player: AVPlayer!
    var avpController = AVPlayerViewController()
    
    var userModel : UserModel! {
        didSet{
//            self.imageViewBackground.image = UIImage(named:String(Int(1 + arc4random() % (8 - 1))))
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(CustomView.className, owner: self, options: nil)
        contentView.fixInView(self)
        btnReply.isHidden = true
//        player = AVPlayer(url: playerUrl)
//        avpController.player = player
//        avpController.view.frame.size.height = self.frame.size.height
//        avpController.view.frame.size.width = self.frame.size.width
//        avpController.entersFullScreenWhenPlaybackBegins = true
//        avpController.showsPlaybackControls = false
//        
//        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying(note:)),
//                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
//        self.addSubview(avpController.view)
    }
    
//    @objc func playerDidFinishPlaying(note: NSNotification) {
//
//        btnReply.isHidden = false
//        avpController.player?.seek(to: .zero)
//    }
    
    private func attributeStringForModel(userModel: UserModel) -> NSAttributedString{
        
        let attributedText = NSMutableAttributedString(string: userModel.name, attributes: [.foregroundColor: UIColor.white,.font:UIFont.boldSystemFont(ofSize: 25)])
        attributedText.append(NSAttributedString(string: "\nnums :\(userModel.name!) - (nib view)" , attributes: [.foregroundColor: UIColor.white,.font:UIFont.systemFont(ofSize: 18)]))
        return attributedText
    }
    
    
    @IBAction func btnReplyAction(_ sender: UIButton) {
        
    }
    
}

extension UIView{
    
    func fixInView(_ container: UIView!) -> Void{
        
        self.translatesAutoresizingMaskIntoConstraints = false;
        self.frame = container.frame;
        container.addSubview(self);
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}

extension NSObject {
    
    class var className: String {
        return String(describing: self)
    }
}
