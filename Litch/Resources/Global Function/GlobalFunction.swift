//
//  GlobalFunction.swift
//  DontForgot
//
//  Created by apple on 3/21/18.
//  Copyright © 2018 cme. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Alamofire

class GlobalFunction: NSObject {
    
    // MARK: - Network Rechability Manager
    static let manager = NetworkReachabilityManager(host: "https://www.google.com")
    static var messageView = InLineErrorView()
    static var matchView = InLineErrorView()
    static var bgView: UIView!

    
    static func addInLineView(in view: UIView,title: String) {
        self.messageView.frame = view.bounds
        self.messageView.lblMessageTitle.text = title
        view.addSubview(self.messageView)
    }
    
    static func addMatchView(in view: UIView,title: String) {
        self.matchView.frame = view.bounds
        self.matchView.lblMessageTitle.text = title
        view.addSubview(self.matchView)
    }
    
    static func removeInLineView() {
        messageView.removeFromSuperview()
    }
    
    static func removeMatchView() {
        matchView.removeFromSuperview()
    }
    
    static func GlobalPrintLogs(strMessage: Any) {
//        print(strMessage)
    }
    
    static func isNetworkReachable() -> Bool {
        
        if (manager?.isReachable)!{
            return true
        }else{
//            APP_DELEGATE.window?.makeToast(Constant.validationMessage.noInternetMSG.localize())
//            GlobalFunction.hideLoadingIndicator()
            return false
        }
    }
    
//    static func checkForSuccess(aDataDic : [String:Any]?, aStatus: Int, showAlert:Bool = false) -> (Bool,[String:Any]?){
//        guard let aDicResponse = aDataDic else {
//            GlobalFunction.alert(appName, "Something went wrong".localize())
//            return (false,nil)
//        }
//
//        guard aStatus == 1 else {
//            let aMsg = aDicResponse["message"] as! String
//            if showAlert {
//            GlobalFunction.alert(appName, aMsg)
//            }
//            return (false,aDicResponse)
//        }
//        return (true,aDicResponse)
//      }
    
    // MARK: - Compress Image
    
    static func resizeImage(image: UIImage) -> Data {
        var actualHeight: Float = Float(image.size.height)
        var actualWidth: Float = Float(image.size.width)
        let maxHeight: Float = 300.0
        let maxWidth: Float = 400.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 1.0
        //50 percent compression
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x: 0.0, y: 0.0, width: Double(actualWidth), height: Double(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let image = img?.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        //        return UIImage(data: imageData!)!
        return image!
    }
    
    
   static func showLoadingIndicator(color:Int, isBottomRef: Bool = false, isSearch: Bool = false, title: String = "") {
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        APP_DELEGATE.spinnerView = MMMaterialDesignSpinner(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(45), height: CGFloat(45)))
        bgView = UIView(frame: UIScreen.main.bounds)
        
        if title != "" {
            bgView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.8988923373)
        }else {
            bgView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        }
        
        
        let titleLabel = UILabel(frame: CGRect(x: 20, y: UIScreen.main.bounds.size.height / 2 - 80, width: UIScreen.main.bounds.width - 40, height: 45))
        titleLabel.text = title
        titleLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont(name: "Metropolis-SemiBold", size: 17)
        bgView.addSubview(titleLabel)
    
       APP_DELEGATE.spinnerView?.center = UIApplication.shared.windows.first!.center
//        bgView.addSubview(APP_DELEGATE.spinnerView!)
         UIApplication.shared.windows.first!.addSubview(APP_DELEGATE.spinnerView!)
        // Set the line width of the spinner
        APP_DELEGATE.spinnerView?.lineWidth = 3.0
        // Set the tint color of the spinner
        
        switch color {
        case 1:
            APP_DELEGATE.spinnerView?.tintColor = #colorLiteral(red: 0.4549019608, green: 0.3137254902, blue: 0.7882352941, alpha: 1)
            break
        case 2:
            APP_DELEGATE.spinnerView?.tintColor = UIColor.white
            break
        default:
            APP_DELEGATE.spinnerView?.tintColor = UIColor.white
            break
        }
        
        APP_DELEGATE.spinnerView?.hidesWhenStopped = true
        // Add it as a subview
        APP_DELEGATE.window?.addSubview(bgView)
        APP_DELEGATE.spinnerView?.startAnimating()
    }
    
    static func hideLoadingIndicator() {
        UIApplication.shared.endIgnoringInteractionEvents()
        if bgView != nil {
            bgView.removeFromSuperview()
        }
        APP_DELEGATE.spinnerView?.stopAnimating()
    }
    
    // MARK - Print Response
    
    static func printResponce(From response: Any) {
//        guard case UIDevice.isSimulator  = true else { return }
//        print("\n\n=====================================\n\n\(response)")
    }

    // MARK: - Email Validation
    
    static func isValidEmail(email:String) -> Bool
    {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: email)
    }
    
    // MARK: - Set Shadow
    
    static func setShadow(view: UIView,offset: CGSize,radious: CGFloat,opacity: Float) {
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = opacity
        view.layer.shadowOffset = offset
        view.layer.shadowRadius = radious
    }
    
    static func setPressAnimation(on view: UIView, scale: CGFloat = 0.6) {
        UIView.animate(withDuration: 0.5) {
            view.transform = .init(scaleX: scale , y: scale)
        }
    }
    
    static func deInitPressAnimation(from view: UIView) {
        UIView.animate(withDuration: 0.5) {
            view.transform = .identity
        }
    }
    
    static func getAudioduration(for resource: String) -> Double {
        let asset = AVURLAsset(url: URL(fileURLWithPath: resource))
        return Double(CMTimeGetSeconds(asset.duration))
    }
    
}



