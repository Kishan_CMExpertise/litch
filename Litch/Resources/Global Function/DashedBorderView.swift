//
//  DashedBorderView.swift
//  Sportevent
//
//  Created by apple on 01/12/18.
//  Copyright © 2018 CMExpertise Infotech. All rights reserved.
//

import UIKit

class DashedBorderView: UIView {

    let border = CAShapeLayer()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    init() {
        super.init(frame: .zero)
        setup()
    }
    
    func setup() {
        border.strokeColor = UIColor.black.cgColor
        border.fillColor = nil
        border.lineDashPattern = [2, 2]
        self.layer.addSublayer(border)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        border.path = UIBezierPath(roundedRect: self.bounds, cornerRadius:8).cgPath
        border.frame = self.bounds
    }

}
