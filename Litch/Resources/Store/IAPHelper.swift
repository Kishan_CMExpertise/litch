

import StoreKit
import KeychainAccess

public typealias ProductIdentifier = String
public typealias ProductsRequestCompletionHandler = (_ success: Bool, _ products: [SKProduct]?) -> ()

open class IAPHelper : NSObject  {
  
    fileprivate let productIdentifiers: Set<ProductIdentifier>
    fileprivate var purchasedProductIdentifiers = Set<ProductIdentifier>()

    fileprivate var productsRequest: SKProductsRequest?
    fileprivate var productsRequestCompletionHandler: ProductsRequestCompletionHandler?

    static let IAPHelperPurchaseNotification = "IAPHelperPurchaseNotification"
    static let IAPHelperRestoreNotification = "IAPHelperRestoreNotification"
    static let IAPHelperErrorNotification = "IAPHelperErrorNotification"
  
    fileprivate let keychain = Keychain(service: "com.litch.pitchyourlove.products").synchronizable(true)
    
    public init(productIds: Set<ProductIdentifier>) {
        self.productIdentifiers = productIds

        for productIdentifier in productIds {
            if let date = keychain[attributes: productIdentifier]?.creationDate {
                purchasedProductIdentifiers.insert(productIdentifier)
                GlobalFunction.GlobalPrintLogs(strMessage: "Previously purchased: \(productIdentifier) on \(date)")
            } else {
                GlobalFunction.GlobalPrintLogs(strMessage: "Not purchased: \(productIdentifier)")
            }
        }

        super.init()

        SKPaymentQueue.default().add(self)
    }
}

// MARK: - StoreKit API
extension IAPHelper {
  
    public func requestProducts(_ completionHandler: @escaping ProductsRequestCompletionHandler) {
        productsRequest?.cancel()
        productsRequestCompletionHandler = completionHandler

        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        productsRequest?.delegate = self
        productsRequest?.start()
    }

    public func buyProduct(_ product: SKProduct) {
        GlobalFunction.GlobalPrintLogs(strMessage: "Buying \(product.productIdentifier)...")
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }

    public func isProductPurchased(_ productIdentifier: ProductIdentifier) -> Bool {
        return purchasedProductIdentifiers.contains(productIdentifier)
    }
  
    public class func canMakePayments() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }
  
    public func restorePurchases() {
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
}

// MARK: - SKProductsRequestDelegate
extension IAPHelper: SKProductsRequestDelegate {
    
    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        GlobalFunction.GlobalPrintLogs(strMessage: "Loaded list of products...")
        let products = response.products
        productsRequestCompletionHandler?(true, products)
        clearRequestAndHandler()

        for product in products {
             GlobalFunction.GlobalPrintLogs(strMessage: "Found product: \(product.productIdentifier) - \(product.localizedTitle) - \(product.priceLocalised))")
        }
    }

    public func requestDidFinish(_ request: SKRequest) {
        GlobalFunction.GlobalPrintLogs(strMessage: "request requestDidFinish - \(request.description)")
    }
    
    public func request(_ request: SKRequest, didFailWithError error: Error) {
        GlobalFunction.GlobalPrintLogs(strMessage: "Failed to load list of products.")
        GlobalFunction.GlobalPrintLogs(strMessage: "Error: \(error.localizedDescription)")
        productsRequestCompletionHandler?(false, nil)
        clearRequestAndHandler()
    }

    fileprivate func clearRequestAndHandler() {
        productsRequest = nil
        productsRequestCompletionHandler = nil
    }
}

// MARK: - SKPaymentTransactionObserver

extension IAPHelper: SKPaymentTransactionObserver {
  
    public func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        if queue.transactions.count == 0
        {
            print("Nothing to restore...")
            GlobalFunction.hideLoadingIndicator()
        }
    }
    
    public func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        print("Nothing to restore...")
        GlobalFunction.hideLoadingIndicator()
    }
    
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
          switch (transaction.transactionState) {
          case .purchased:
            completeTransaction(transaction)
            break
          case .failed:
            failedTransaction(transaction)
            break
          case .restored:
            restoreTransaction(transaction)
            queue.finishTransaction(transaction)
            break
          case .deferred:
//            print("defereed")
            break
          case .purchasing:
            break
          }
        }
    }
  
    fileprivate func completeTransaction(_ transaction: SKPaymentTransaction) {
        GlobalFunction.GlobalPrintLogs(strMessage: "completeTransaction...")
        deliverPurchaseNotificatioForIdentifier(transaction.payment.productIdentifier, transaction)
        SKPaymentQueue.default().finishTransaction(transaction)
    }
  
    fileprivate func restoreTransaction(_ transaction: SKPaymentTransaction) {
        guard let productIdentifier = transaction.original?.payment.productIdentifier else {
            return
        }
//        print(productIdentifier)
        
        GlobalFunction.GlobalPrintLogs(strMessage: "restoreTransaction... \(productIdentifier)")
        SKPaymentQueue.default().finishTransaction(transaction)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.deliverRestoreNotificatioForIdentifier(productIdentifier, transaction)
//        }

    }
  
    fileprivate func failedTransaction(_ transaction: SKPaymentTransaction) {
        GlobalFunction.GlobalPrintLogs(strMessage: "failedTransaction..., \(String(describing: transaction.error?.localizedDescription))")
        SKPaymentQueue.default().finishTransaction(transaction)
        NotificationCenter.default.post(name: Notification.Name(rawValue: IAPHelper.IAPHelperErrorNotification), object: transaction.error)
    }
  
    fileprivate func deliverPurchaseNotificatioForIdentifier(_ identifier: String?, _ transaction: SKPaymentTransaction) {
        guard let identifier = identifier else {
            return
        }

        purchasedProductIdentifiers.insert(identifier)
        keychain[identifier] = "purchased"
        NotificationCenter.default.post(name: Notification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification), object: transaction)
    }
    
    fileprivate func deliverRestoreNotificatioForIdentifier(_ identifier: String?, _ transaction: SKPaymentTransaction) {
        guard let identifier = identifier else {
            return
        }
        
        purchasedProductIdentifiers.insert(identifier)
        keychain[identifier] = "purchased"
        NotificationCenter.default.post(name: Notification.Name(rawValue: IAPHelper.IAPHelperRestoreNotification), object: transaction)
    }
}


extension SKProduct {
    var priceLocalised: String {
        let numberFormatter = NumberFormatter()
        numberFormatter.formatterBehavior = .behavior10_4
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = self.priceLocale
        return numberFormatter.string(from: self.price) ?? ""
    }
}
