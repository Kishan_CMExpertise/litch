//
//  StoreManager.swift
//  FakeCall
//
//  Created by Maslov Sergey on 28.04.16.
//  Copyright © 2016 Home. All rights reserved.
//

import Foundation
import StoreKit

protocol StoreManagerProtocol: class {
    func didPurchase(purchaseType: String, transaction: SKPaymentTransaction)
    func didError(_ error: Error)
}


class StoreManager {
    static let shared : StoreManager = {
        let instance = StoreManager()
        return instance
    }()
    
    weak var delegate: StoreManagerProtocol?

    var products = [SKProduct]()
    
    var accessDoubleTime : SKProduct? {
        guard products.filter({ $0.productIdentifier == StoreProducts.litchDoubleTime }).count != 0 else{
            return nil
        }
        return products.filter({ $0.productIdentifier == StoreProducts.litchDoubleTime }).first
    }

    //MARK: Shared Instance
    fileprivate init() {
        NotificationCenter.default.addObserver(self, selector: #selector(StoreManager.handlePurchaseNotification(_:)),
                                                         name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification),
                                                         object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(StoreManager.handleRestoreNotification(_:)),
                                                         name: NSNotification.Name(rawValue: IAPHelper.IAPHelperRestoreNotification),
                                                         object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(StoreManager.handleErrorNotification(_:)),
                                               name: NSNotification.Name(rawValue: IAPHelper.IAPHelperErrorNotification),
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func updateProducts() {
        StoreProducts.store.requestProducts{success, products in
            if success {
                self.products = products!
//                print(self.products)
            }
        }
    }
    
    func buyAccessCode() {
        
        guard self.products.count != 0 else {
            APP_DELEGATE.window?.makeToast("IAP is not awailable")
            GlobalFunction.hideLoadingIndicator()
            return
        }
        
        if let product = accessDoubleTime {
            StoreProducts.store.buyProduct(product)
        }
    }
    
        
    func restorePurchases() {
        StoreProducts.store.restorePurchases()
    }
    
    @objc func handlePurchaseNotification(_ notification: Notification) {
//        guard let productID = notification.object as? String else {
//            return
//        }
//
        
        guard let transaction = notification.object as? SKPaymentTransaction else {
            return
        }
        
        let productID = transaction.payment.productIdentifier
        
        for (_, product) in StoreManager.shared.products.enumerated() {
            guard product.productIdentifier == productID else {
                continue
            }
            delegate?.didPurchase(purchaseType: productID, transaction: transaction)
            GlobalFunction.GlobalPrintLogs(strMessage: "Just bought it")
        }

    }
    
    @objc func handleRestoreNotification(_ notification: Notification) {
        guard let transaction = notification.object as? SKPaymentTransaction else {
            return
        }
        
        let productID = transaction.payment.productIdentifier
        
        for (_, product) in StoreManager.shared.products.enumerated() {
            guard product.productIdentifier == productID else {
                continue
            }
            delegate?.didPurchase(purchaseType: productID, transaction: transaction)
            GlobalFunction.GlobalPrintLogs(strMessage: "Just restore it")
        }
    }
    
    @objc func handleErrorNotification(_ notification: Notification) {
        guard let error = notification.object as? Error else {
            return
        }
        delegate?.didError(error)
    }
}
