//
//  InLineErrorView.swift
//  Sportevent
//
//  Created by mac on 11/12/18.
//  Copyright © 2018 CMExpertise Infotech. All rights reserved.
//

import UIKit

class InLineErrorView: UIView {

    @IBOutlet weak var lblMessageTitle: UILabel!
    @IBOutlet var ContentView: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commitInit()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        commitInit()
    }
    
    private func commitInit() {
        Bundle.main.loadNibNamed("InLineErrorView", owner: self, options: nil)
        addSubview(ContentView)
        ContentView.frame = bounds
        ContentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
}
