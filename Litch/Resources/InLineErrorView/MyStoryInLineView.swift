//
//  MyStoryInLineView.swift
//  KinoKeApp
//
//  Created by mac on 12/07/19.
//  Copyright © 2019 CMExpertise Infotech pvt. ltd. All rights reserved.
//

import UIKit

class MyStoryInLineView: UIView {

    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblShareKinoke: UILabel!
    @IBOutlet weak var lblTapNew: UILabel!
    @IBOutlet weak var lblGuestStorySeeIt: UILabel!
  
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commitInit()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        commitInit()
    }
    
    private func commitInit() {
        Bundle.main.loadNibNamed("MyStoryInLineView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

}
