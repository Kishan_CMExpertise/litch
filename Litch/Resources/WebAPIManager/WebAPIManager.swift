//
//  WebAPIManager.swift
//  WorxSR
//
//  Created by mac on 7/20/18.
//  Copyright © 2018 CMExpertise Infotech. All rights reserved.
//

import Foundation
import Alamofire

class WebAPIManager: NSObject {
    //MARK:- API Call
//
    class func makeAPIRequest(method: HTTPMethod = .post, isFormDataRequest: Bool,isAuthenticationToken : Bool = false, isContainXAPIToken: Bool, isContainContentType: Bool, path: String, params: Parameters, bsaeUrl baseUrl: String = baseUrl,usersKey : String = "", completion: @escaping (_ response: [AnyHashable: Any],_ status: Int) -> Void) {

        if !GlobalFunction.isNetworkReachable() {
            var dict = [AnyHashable: Any]()
            dict["message"] = Constant.validationMessage.noInternetMSG
            dict["status"] = 0
            completion(dict, 0)
            return
        }

        let baseURL = baseUrl + path

//                GlobalFunction.printResponce(From: "Request URL = \(baseURL) parameters = \(params) API Token = \(User.sharedInstance.token ?? "")")
        var customHeader:[String:String] = [:]
        if isContainContentType {
            customHeader["Content-Type"] = "application/x-www-form-urlencoded"
        }
        
        if usersKey != "" {
            customHeader["usersKey"] = usersKey
        }
        
        if isAuthenticationToken {
            
            guard let decoded  = Constant.UserDefault.object(forKey: Constant.UserDefaultKeys.currentUserModel) as? Data else { return }
            guard let decodedUser = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? LoginModel else { return  }
            customHeader["Authorization"] = "Bearer \(decodedUser.token ?? "")"
        }

        var encoding: ParameterEncoding = JSONEncoding.default
        if isFormDataRequest {
            encoding = URLEncoding.default
        }

        Alamofire.request(baseURL, method: method, parameters: params, encoding: encoding, headers: customHeader).responseJSON { (response:DataResponse<Any>) in

            GlobalFunction.printResponce(From: "\nAPI :- \(baseURL) \nParameters :- \(params) \n\n\(response.result.value ?? "")\n")
            GlobalFunction.hideLoadingIndicator()
            switch(response.result) {
            case .success(_):
                if let responseData = response.result.value as? [AnyHashable : Any] {
                    completion(responseData,1)
                }
                break
            case .failure(_):
                var dict = [AnyHashable: Any]()
                dict["message"] = "Oops! Something went wrong. Please try again."
                dict["status"] = 0
                completion(dict, 0)
                break
            }
        }
    }

    class func makeMultipartRequestToUploadImages(method: HTTPMethod = .post, isFormDataRequest: Bool,isAuthenticationToken : Bool = false, isContainXAPIToken: Bool, isContainContentType: Bool, path: String, parameters: [String:String], imagesData: [[String: [Data]]], bsaeUrl: String = baseUrl, completion: @escaping (_ response: [AnyHashable: Any],_ status: Int) -> Void) {

        if !GlobalFunction.isNetworkReachable() {
            return
        }
        let url = baseUrl + path

        //        GlobalFunction.printResponce(From: "Request URL = \(url) parameters = \(parameters) API Token = \(User.sharedInstance.token ?? "")")

        var customHeader:[String:String] = [:]
        if isContainContentType {
            customHeader["Content-Type"] = "application/x-www-form-urlencoded"
        }
        
        if isAuthenticationToken {
            
            guard let decoded  = Constant.UserDefault.object(forKey: Constant.UserDefaultKeys.currentUserModel) as? Data else { return }
            guard let decodedUser = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? LoginModel else { return  }
            
            customHeader["Authorization"] = "Bearer \(decodedUser.token ?? "")"
        }
        
        var encoding: ParameterEncoding = JSONEncoding.default
        if isFormDataRequest {
            encoding = URLEncoding.default
        }

        Alamofire.upload(multipartFormData: { multipartFormData in
            //                multipartFormData.append(self.imageData, withName: "user_image", fileName: "picture.png", mimeType: "image/png")
            if imagesData.count > 0 {
                for data in imagesData {
                    if data.count > 0 {
                        let key = data.keys.first
                        let imagesData = data[key!]
                        let paramKey = key!
                        
                        if paramKey == "video" {
                            for dataToUpload in imagesData! {
                                multipartFormData.append(dataToUpload, withName: paramKey, fileName: "\(Date().timeIntervalSince1970).mp4", mimeType:  "\(Date().timeIntervalSince1970)/mp4")
                            }
                        }
                        
                        if paramKey == "image" {
                            for dataToUpload in imagesData! {
                                multipartFormData.append(dataToUpload, withName: paramKey, fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                            }
                        }
                        
                    }
                }
            }

            for (key, value) in parameters {
                multipartFormData.append(value.data(using: .utf8)!, withName: key )
            }
        }, to: url, method: method, headers: customHeader, encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response:DataResponse<Any>) in
                    //                    GlobalFunction.printResponce(From: "API NAME \(path) :- \(response.result.value ?? "")")
                    GlobalFunction.hideLoadingIndicator()
                    switch(response.result) {
                        
                    case .success(_):
                        if let responseData = response.result.value as? [AnyHashable : Any] {
                            completion(responseData,1)
                        }
                        break
                        
                    case .failure(_):
                        var dict = [AnyHashable: Any]()
                        dict["message"] = "Oops! Something went wrong. Please try again."
                        dict["status"] = 0
                        completion(dict, 0)
                        break
                    }
                }).uploadProgress { progress in // main queue by default
                    //                    GlobalFunction.printResponce(From: "Upload Progress: \(progress.fractionCompleted)")
                }
                return
            case .failure(let _):
                var dict = [AnyHashable: Any]()
                dict["message"] = "Oops! Something went wrong. Please try again."
                dict["success"] = 0
                completion(dict, 0)
            }
        })
    }
    
    
    class func makeMultipartRequestToUploadVideo(method: HTTPMethod = .post, isFormDataRequest: Bool,isAuthenticationToken : Bool = false, isContainXAPIToken: Bool, isContainContentType: Bool, path: String, parameters: [String:String], videoData: [[String: [Data]]],authToken : String, bsaeUrl: String = baseUrl, completion: @escaping (_ response: [AnyHashable: Any],_ status: Int) -> Void) {
        
        if !GlobalFunction.isNetworkReachable() {
            return
        }
        let url = baseUrl + path
        
        //        GlobalFunction.printResponce(From: "Request URL = \(url) parameters = \(parameters) API Token = \(User.sharedInstance.token ?? "")")
        
        var customHeader:[String:String] = [:]
        if isContainContentType {
            customHeader["Content-Type"] = "application/x-www-form-urlencoded"
        }
        
        if isAuthenticationToken {
            
            guard let decoded  = Constant.UserDefault.object(forKey: Constant.UserDefaultKeys.currentUserModel) as? Data else { return }
            guard let decodedUser = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? UserModel else { return  }
            customHeader["Authorization"] = "Bearer \(authToken)"
        }
        
        var encoding: ParameterEncoding = JSONEncoding.default
        if isFormDataRequest {
            encoding = URLEncoding.default
        }
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            //                multipartFormData.append(self.imageData, withName: "user_image", fileName: "picture.png", mimeType: "image/png")
            if videoData.count > 0 {
                for data in videoData {
                    if data.count > 0 {
                        let key = data.keys.first
                        let imagesData = data[key!]
                        let paramKey = key!
                        for dataToUpload in imagesData! {
                            
                            if paramKey == "image" {
                                multipartFormData.append(dataToUpload, withName: paramKey, fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                            }
                            
                            if paramKey == "video" {
                            multipartFormData.append(dataToUpload, withName: paramKey, fileName: "\(Date().timeIntervalSince1970).mp4", mimeType:  "\(Date().timeIntervalSince1970)/mp4")
                            }
                        }
                    }
                }
            }
            
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: .utf8)!, withName: key )
            }
        }, to: url, method: method, headers: customHeader, encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response:DataResponse<Any>) in
                    //                    GlobalFunction.printResponce(From: "API NAME \(path) :- \(response.result.value ?? "")")
                    GlobalFunction.hideLoadingIndicator()
                    switch(response.result) {
                        
                    case .success(_):
                        if let responseData = response.result.value as? [AnyHashable : Any] {
                            completion(responseData,1)
                        }
                        break
                        
                    case .failure(_):
                        var dict = [AnyHashable: Any]()
                        dict["message"] = "Oops! Something went wrong. Please try again."
                        dict["status"] = 0
                        completion(dict, 0)
                        break
                    }
                }).uploadProgress { progress in // main queue by default
                    //                    GlobalFunction.printResponce(From: "Upload Progress: \(progress.fractionCompleted)")
                }
                return
            case .failure( _):
                var dict = [AnyHashable: Any]()
                dict["message"] = "Oops! Something went wrong. Please try again."
                dict["success"] = 0
                completion(dict, 0)
            }
        })
    }
    

    class func makeMultipartRequestToUploadAudio(method: HTTPMethod = .post, isFormDataRequest: Bool,isAuthenticationToken : Bool = false, isContainXAPIToken: Bool, isContainContentType: Bool, path: String, parameters: [String:String], audioData: [[String: [Data]]], bsaeUrl: String = baseUrl, completion: @escaping (_ response: [AnyHashable: Any],_ status: Int) -> Void) {
        
        if !GlobalFunction.isNetworkReachable() {
            return
        }
        
        let url = baseUrl + path
        
        //        GlobalFunction.printResponce(From: "Request URL = \(url) parameters = \(parameters) API Token = \(User.sharedInstance.token ?? "")")
        
        var customHeader:[String:String] = [:]
        if isContainContentType {
            customHeader["Content-Type"] = "application/x-www-form-urlencoded"
        }
        
        if isAuthenticationToken {
            
            guard let decoded  = Constant.UserDefault.object(forKey: Constant.UserDefaultKeys.currentUserModel) as? Data else { return }
            guard let decodedUser = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? UserModel else { return  }
            customHeader["Authorization"] = "Bearer \("")"
        }
        
        var encoding: ParameterEncoding = JSONEncoding.default
        if isFormDataRequest {
            encoding = URLEncoding.default
        }
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            //                multipartFormData.append(self.imageData, withName: "user_image", fileName: "picture.png", mimeType: "image/png")
            if audioData.count > 0 {
                for data in audioData {
                    if data.count > 0 {
                        let key = data.keys.first
                        let imagesData = data[key!]
                        let paramKey = key!
                        for dataToUpload in imagesData! {
                            multipartFormData.append(dataToUpload, withName: paramKey, fileName: "\(Date().timeIntervalSince1970).m4a", mimeType:  "audio/m4a")
                        }
                    }
                }
            }
            
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: .utf8)!, withName: key )
            }
        }, to: url, method: method, headers: customHeader, encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response:DataResponse<Any>) in
                    //                    GlobalFunction.printResponce(From: "API NAME \(path) :- \(response.result.value ?? "")")
                    GlobalFunction.hideLoadingIndicator()
                    switch(response.result) {
                        
                    case .success(_):
                        if let responseData = response.result.value as? [AnyHashable : Any] {
                            completion(responseData,1)
                        }
                        break
                        
                    case .failure(_):
                        var dict = [AnyHashable: Any]()
                        dict["message"] = "Oops! Something went wrong. Please try again."
                        dict["status"] = 0
                        completion(dict, 0)
                        break
                    }
                }).uploadProgress { progress in // main queue by default
                    //                    GlobalFunction.printResponce(From: "Upload Progress: \(progress.fractionCompleted)")
                }
                return
            case .failure( _):
                var dict = [AnyHashable: Any]()
                dict["message"] = "Oops! Something went wrong. Please try again."
                dict["success"] = 0
                completion(dict, 0)
            }
        })
    }
    
    static func showErrorRespose(errorResponse: [AnyHashable: Any]?) {

        DispatchQueue.main.async {

            if errorResponse != nil {
//                APP_DELEGATE.window?.makeToast(errorResponse?["message"] as! String)
            }

            GlobalFunction.hideLoadingIndicator()
        }
    }
    
    
}
