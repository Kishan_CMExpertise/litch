//
//  Message.swift
//  ReviewNRefer
//
//  Created by apple on 6/12/18.
//  Copyright © 2018 cme. All rights reserved.
//

import Foundation


class Messages : NSObject {
    
    static let enterFirstName = "Please enter first name"
    static let enterFullName = "Please enter full name"
    static let enterLastName = "Please enter last name"
    static let enterValidEmail = "Please enter valid email"
    static let enterEmail = "Please enter email"
    static let enterCity = "Please enter city"
    static let enterState = "Please enter state"
    static let enterPincode = "Please enter pincode"
    static let enterCountry = "Please enter country"
    static let passwordminimum = "Please enter minimum 6 digits password."
    static let enterUserName = "Please enter user name"
    static let enterpassword = "Please enter password"
    static let selectLocation = "Please select your delivery location"
    static let enterMobileNo = "Please enter mobile number"
    static let enterMobileRange = "Please enter 10 to 15 digit phone number"
    static let notRegister = "Sorry! user not found in system"
    static let emailpasswordWrong = "Email address and password does not match"
    static let oldPassword = "Please enter old password"
    static let confirmPassword = "Please enter confirm password"
    static let enterAddress = "Please enter address"
    static let passNotMatch = "Password and confirm password does not match"
    static let passMatch = "New password and confirm password does not match"
    static let enterCategory = "Please enter Category"
    static let enterDetail = "Please enter Detail"
    static let netMessage = "Internet is not available"
    static let selectCategory = "Please select category"
    static let enterdescription = "Please enter description"
    static let enterOldPassword = "Enter old password"
    static let enterCurrentPass = "Please enter current password."
    static let oldpasswordminimum = "Old Password must have 6 or more characters"
    static let enterNewPassword = "Please enter new password."
    static let newPasswordMinimum = "New Password must have 6 or more characters"
    static let enterConfirmPassword = "Please enter confirm password"
    static let confirmPasswordMinimum = "Confirm Password must have 6 or more characters"
    static let newPassConfirmPassNotMatch = "New password and confirm password not match"
    static let enterOnlyName = "Enter Name"
    static let enterFeedback = "Please Enter Feedback"
    static let enterreview = "Please enter review"
    static let enterCountryCode = "Please select country code"
    static let checkoutAlert = "Please add at least one item in  cart to checkout"
    static let clearAlert = "No item availabel in cart"
    static let addressAlert = "Please enter atleast one address!."
}

