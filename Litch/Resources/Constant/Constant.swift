//
//  Constant.swift
//  DontForgot
//
//  Created by apple on 3/8/18.
//  Copyright © 2018 cme. All rights reserved.
//

import Foundation
import UIKit

let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate
let appName = "Litch"
let baseUrl = "https://litch.herokuapp.com/"
let deviceID = UIDevice.current.identifierForVendor?.uuidString
let DEVICE_ID = UIDevice.current.identifierForVendor!.uuidString

var currentAccountInfo : AccountInfoModel!

class Constant: NSObject {
    
    static let UserDefault = UserDefaults.standard
    static let AppDel = UIApplication.shared.delegate as! AppDelegate
    static let deviceID = UIDevice.current.identifierForVendor?.uuidString
    
    struct Key{
    }
    
    struct Screen {
        static let Width         = UIScreen.main.bounds.size.width
        static let Height        = UIScreen.main.bounds.size.height
        static let Max_Length    = max(Width, Height)
        static let Min_Length    = min(Width, Height)
        
        static let iPhone                                      = UIDevice.current.userInterfaceIdiom == .phone
        static let iPad                                         = (UIDevice.current.userInterfaceIdiom == .pad)
        static let iPhone_4_Screen                            = Max_Length >= 480.0 && Max_Length < 568.0
        static let iPhone_5_Screen                            = Max_Length >= 568.0 && Max_Length < 667.0
        static let iPhone_6_OR_7_Screen                       = Max_Length >= 667.0 && Max_Length < 736.0
        static let iPhone_6Plus_OR_7Plus_Screen               = Max_Length >= 736.0 && Max_Length < 812.0
        static let iPhone_X_OR_XS_Screen                      = Max_Length >= 812.0 && Max_Length < 896.0
        static let iPhone_XR_OR_XS_MAX_Screen                 = Max_Length >= 896.0 && Max_Length < 1024.0
        static let iPad_Mini_OR_iPad_Air_OR_iPad_Pro_9_7      = Max_Length >= 1024.0 && Max_Length < 1112.0
        static let iPad_Pro_10_5                              = Max_Length >= 1112.0 && Max_Length < 1366.0
        static let iPad_Pro_12_9                              = Max_Length == 1366.0
        static let TV                                         = UIDevice.current.userInterfaceIdiom == .tv
        static let Car_Play                                   = UIDevice.current.userInterfaceIdiom == .carPlay
    }
    
    struct CategoryImage {
        static let cricket = #imageLiteral(resourceName: "cricket_icon")
        static let football = #imageLiteral(resourceName: "football_icon")
        static let basketball = #imageLiteral(resourceName: "basketball_active")
        static let defaultImage = #imageLiteral(resourceName: "game")
    }
    
    struct Color {
        static let themeColor = #colorLiteral(red: 0.2823529412, green: 0.231372549, blue: 0.4470588235, alpha: 1)
        static let  gradientColors = [#colorLiteral(red: 0.4549019608, green: 0.3137254902, blue: 0.7882352941, alpha: 1),#colorLiteral(red: 0.5019607843, green: 0.9294117647, blue: 0.9215686275, alpha: 1)]
    }
    
    struct locationKeys {
        static let lat = "selectedLat"
        static let lng = "selectedLng"
        static let city = "selectedCity"
    }
    
    struct params {
        static let first = "first"
        static let last = "last"
        static let email = "email"
        static let password = "password"

        static let likedUsersId = "likedUsersId"
        static let discardedUsersIds = "discardedUsersIds"
        static let matchId = "matchId"
        static let messageText = "messageText"
        static let flaggedUserId = "flaggedUserId"
        
        static let name = "name"
        static let sex = "sex"
        static let interestedIn = "interestedIn"
        static let year = "year"
        static let month = "month"
        static let day = "day"
        static let question = "question"
        static let location = "location"
        static let video = "video"
        static let image = "image"
    }
    
    struct mediaType {
        static let video = "video"
        static let audio = "audio"
        static let image = "image"
    }
    
    struct message {
        static let invitedToThisStory = "Invited to this story"
        static let previouslyInvitedToYourStories = "Previously invited to your stories"
        static let otherPeopleOnKinoke = "Other people on Kinoke"
        static let yourContacts = "Your contacts"
    }
    
    struct API {
       
        static let login = "login"
        static let signup = "signup"
        static let like = "like"
        static let flagUser = "flagUser"
        static let getLikes = "getLikes"
        static let discard = "discard"
        static let browseLitch = "browseLitch"
        static let finishSignup = "finishSignup"
        static let getMatches = "getMatches"
        static let getAccountInfo = "getAccountInfo"
        static let getChatHistory = "getChatHistory"
        static let createChatMessage = "createChatMessage"
        
        static let reset = "reset"
        static let createPurchase = "createPurchase"
    }
    
    struct cellIdentifier {
        static let StoriesTVC = "StoriesTVC"
        static let CommentTVC = "CommentTVC"
        static let InviteContactTVC = "InviteContactTVC"
        static let InviteListTVC = "InviteListTVC"
        static let SkeletonTVC = "SkeletonTVC"
        
    }
    
    struct buttonTitle {
        
        static let deleteThisStory = "Are you sure you want to delete this story?"
        
        static let performAction = "Perform action"
        static let deleteStory = "Delete Story"
        static let removeStory = "Remove story"
        static let reportUser = "Report this user"
        static let Cancel = "Cancel"
        
        static let chooseOption = "Choose Option"
        static let chooseAnOption = "Choose an option"
        static let captureVideo = "Capture video"
        static let selectFromGallary = "Select from gallary"
        static let captureImage = "Capture image"
        static let OK = "OK"
        static let logout = "Logout"
        static let Kinvite = "Kinvite"
        static let yesRemove = "Yes, remove."
        static let InviteToKinoke = "Invite to Kinoke"
    }
    
    struct viewController {
        static let saveMediaVC = "SaveMediaVC"
        static let ImagePreviewVC = "ImagePreviewVC"
    }
    
    struct StoryBoard {
        static let main = "Main"
    }
    
    struct UserDefaultKeys {
         static let currentUserModel = "currentUserModel"
        static let globalSelectedAlbumData = "globalSelectedAlbumData"
    }
    
    struct segueIdentifier {
        static let segueStarteToLogSignUp = "segueStarteToLogSignUp"
        static let segueLoginToTab = "segueLoginToTab"
        static let segueRegistrationToInstruction = "segueRegistrationToInstruction"
        static let segueInstructionToQueList = "segueInstructionToQueList"
        
        static let segueQueListToPopup = "segueQueListToPopup"
        static let segueQueListToVideoRecording = "segueQueListToVideoRecording"
        static let segueVideoPlayerToGenderInterest = "segueVideoPlayerToGenderInterest"
        
        static let segueGenderToNameAgeInfo = "segueGenderToNameAgeInfo"
        
        static let segueNameDateToHomeTab = "segueNameDateToHomeTab"
        static let segueFavoriteToChat = "segueFavoriteToChat"
        
        static let segueProfileToMyLitch = "segueProfileToMyLitch"
        
        static let segueFavoriteToLikeLitch = "segueFavoriteToLikeLitch"
        
        static let segueVideoToPermission = "segueVideoToPermission"
        
        static let segueLoginToForgotPassword = "segueLoginToForgotPassword"
    }
    
    struct addressBookKey {
        static let fname = "first_name"
        static let lname = "last_name"
        static let mobile = "mobile"
        static let mobile1 = "phone_number1"
        static let identifier = "identifier"
    }
    
    struct serverAddressBook {
        static let appUser      = "app_user"
        static let appUsernmae  = "app_username"
        static let firstName    = "first_name"
        static let fullName     = "full_name"
        static let id           = "id"
        static let imageUrl     = "image_url"
        static let lastName     = "last_name"
        static let phoneNumber  = "phone_number"
        static let tblID        = "TblID"
        static let isSelected        = "is_selected"
        static let delete_event = "delete_event"
        static let event_update = "event_update"
        static let skill_update = "skill_update"
    }

    
    struct notificationName {
//        static let refreshMenu = Notification.Name.init("refreshMenu")
    }
    
    struct validationMessage {
        
         static let selectYourGender = "Please select your gender"
        static let selectInterestGender = "Please select your interest gender"
        
        static let noInternetMSG = "Please Check Internet Connection"
        static let locationalert = "Please Enable Location Service To Use Services"
        static let isValidName = "Please enter name"
        static let isValidFirstname = "Please enter first name"
        static let isValidLastname = "Please enter last name"
        static let isValidUserName = "Please enter username"
        static let isEmailEmpty = "Please enter email address"
        static let isEmailInvalid = "Please enter valid email address"
        static let isValidNewPassword = "Please enter new password"
        static let isValidPassword = "Please enter password"
        static let isValidPasswordRange = "Password must have 6 to 15 characters"
        static let isValidBirthDate = "Please Select birth date"
        static let isCheckAccessCode = "Please enter access code"
        
        
        static let isValidPasswordRangeTo6 = "Password must have 6 characters"
        
        static let isValidNewPasswordRange = "New password must have 6 to 15 characters"
        static let isConfirmPasswordEmpty = "Please enter confirm password"
        static let isValidConfirmPasswordRange = "Confirm password must have 6 to 15 characters"
        static let isConfirmPasseordMatch = "New password and confirm password does not match"
        static let isOldPasswordEmpty = "Please enter old password"
        static let isMobileEmpty = "Please enter phone"
        static let isValidMobileRange = "Phone number should contain 8 to 15 digits"
        static let isRememberSelected = "Please select remember me"
        static let isLocationEmpty = "Please enter location"
        static let isSelectCountryCode = "Please select country code"
        static let isValidOldPassword = "Old password not match"
        static let isNewPasswordDiff = "Old password and new password same"
        static let mobileWithoutPlusDigit = "Please enter mobile without + Digit"
        
        static let passConPassDiff = "Password and confirm password are different"
        
        static let noStoryfound = "No story found"
        static let noCommentYet  = "No comment yet!"
        
        static let wouldYouLikeToLogout = "Would you like to logout?"
        
        static let pleaseEnterStoryTitle = "Please enter story title"
        
        static let pleaseSelectAnybodyToInvite = "You haven’t selected anybody to invite!"
        
        static let youHaveTypeSomething = "You have to type something!"
        static let PleaseHangOnMinute = "Please hang on a minute…"
        
    }
    
    struct Alert{
        static let enterEmail = "Please enter your email address."
        static let enterValidEmail = "Please enter valid email address."
        static let enterPassword = "Please enter password."
        static let enterValidPassword = "Please enter password greater than six digits."
        static let noNetwork = "No Internet Connection"
        static let blankFirstName = "First name should not be blank."
        static let blankLastName = "Last name should not be blank."
        static let blankPhoneNumber = "Phone number should not be blank."
        static let shortPhone = "Your phone number is too short."
        static let sixDigitPhone = "Please enter password greater than six digits."
        static let confirmPass = "Confirm password should not be blank."
        static let passNotMatch = "Your confirm password did not match with current password. Please enter valid password."
        static let nameBlank = "Name should not be blank."
        static let addressBlank = "Address should not be blank."
        static let landmarkBlank = "Landmark should not be blank."
        static let cityBlank = "City should not be blank.."
        static let pincodeBlank = "Pin code should not be blank."
        static let stateBlank = "State should not be blank."
        static let countryBlank = "Country should not be blank."
        static let phoneShort = "Your phone number is too short."
        static let enterCurrentPass = "Enter your current password."
        static let enterNewPass = "Enter your new password."
        static let confirmNewPass = "Confirm your new password."
        static let selectDeliveryAddress = "Please select your product delivery address."
        static let selectDeliveryTime = "Please select your product delivery time."
        static let selectPaymentMethod = "Please select payment Method."
        static let noGallery = "No Gallery"
        static let noCamera = "No Camera"
        static let deviceHasNoGallery = "Sorry, this device has no Gallery"
        static let deviceHasNoCamera = "Sorry, this device has no camera"
        
    }
}
