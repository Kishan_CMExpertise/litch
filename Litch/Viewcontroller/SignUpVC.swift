//
//  SignUpVC.swift
//  Litch
//
//  Created by Kishan Suthar on 29/06/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit
import Alamofire

var globalSigupModel : SignUpModel!

class SignUpVC: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK:- validation for textfield method call
    func validate() -> Bool {
        
        if txtEmail.text == "" {
            self.view.makeToast(Constant.validationMessage.isEmailEmpty)
            return false
        }
        if !GlobalFunction.isValidEmail(email: txtEmail.text ?? "") {
            self.view.makeToast(Constant.validationMessage.isEmailInvalid)
            return false
        }
        if txtPassword.text == "" {
            self.view.makeToast(Constant.validationMessage.isValidPassword)
            return false
        }
        if (txtPassword.text?.count)! < 6  {
            self.view.makeToast(Constant.validationMessage.isValidPasswordRangeTo6)
            return false
        }
        if txtConfirmPassword.text == "" {
            self.view.makeToast(Constant.validationMessage.isValidPassword)
            return false
        }
        if (txtConfirmPassword.text?.count)! < 6  {
            self.view.makeToast(Constant.validationMessage.isValidPasswordRangeTo6)
            return false
        }
        if (txtPassword.text)! != (txtConfirmPassword.text)!   {
            self.view.makeToast(Constant.validationMessage.passConPassDiff)
            return false
        }
        return true
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        
        if validate() {
            
            GlobalFunction.showLoadingIndicator(color: 1)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.SignUpAPICall()
            }
        }
    }
    
    // MARK:- login API method call
    private func SignUpAPICall() {
        
        var params: Parameters = [:]
        params[Constant.params.email]  = txtEmail.text!
        params[Constant.params.password]  = txtPassword.text!
        
        WebAPIManager.makeAPIRequest(isFormDataRequest: true, isContainXAPIToken: false, isContainContentType: true, path:Constant.API.signup, params: params) { (response, status) in
            
            let responseData = SignUpModel(fromDictionary: response as! [String : Any])
            
            if responseData.status == 200 {
                APP_DELEGATE.window?.makeToast(responseData.message)
                globalSigupModel = responseData
                
                let loginMod = LoginModel(fromDictionary: response as! [String: Any])
                loginMod.saveInUserDefault()
                Constant.AppDel.currentLoginUser = loginMod
                APP_DELEGATE.window?.makeToast(responseData.message)
                self.performSegue(withIdentifier: Constant.segueIdentifier.segueRegistrationToInstruction, sender: self)
            }else {
                self.view.makeToast(responseData.message)
            }
        }
    }
}

// MARK:- VCTextFieldDelegate method call
extension SignUpVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtEmail {
            txtPassword.becomeFirstResponder()
            return true
        }else if textField == txtPassword {
            txtConfirmPassword.becomeFirstResponder()
            return true
        }else if textField == txtConfirmPassword {
            txtConfirmPassword.resignFirstResponder()
            return true
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == " " {
            return false
        }
        return true
    }
}
