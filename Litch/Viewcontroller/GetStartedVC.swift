//
//  GetStartedVC.swift
//  Litch
//
//  Created by mac on 13/07/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class GetStartedVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnGetStartedAction(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: Constant.segueIdentifier.segueStarteToLogSignUp, sender: self)
        
        //        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
        //
        //
        //            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
        //                       self.performSegue(withIdentifier: Constant.segueIdentifier.segueStarteToLogSignUp, sender: self)
        //            }
        //        } else {
        //            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
        //                if granted {
        //
        //                    AVAudioSession.sharedInstance().requestRecordPermission { (status) in
        //
        //                        if status {
        //                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
        //                                self.performSegue(withIdentifier: Constant.segueIdentifier.segueStarteToLogSignUp, sender: self)
        //                            }
        //                        }else {
        //                            self.presentCameraSettings()
        //                        }
        //                    }
        //                } else {
        //
        //                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
        //                        self.presentCameraSettings()
        //                    }
        //                }
        //            })
        //        }
    }
    
    func presentCameraSettings() {
        let alertController = UIAlertController(title: "Access required",
                                                message: "You can enable access to camera and microphone permission in privacy settings",
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Close", style: .default))
        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        })
        
        present(alertController, animated: true)
    }
    
}
