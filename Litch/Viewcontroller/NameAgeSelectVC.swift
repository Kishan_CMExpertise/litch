//
//  NameAgeSelectVC.swift
//  Litch
//
//  Created by Kishan Suthar on 26/06/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit
import Alamofire

class NameAgeSelectVC: UIViewController {
    
    let picker = UIImagePickerController()
    fileprivate var alertStyle: UIAlertController.Style = .actionSheet
    var selectedDOB: String = ""
    
    @IBOutlet weak var txtBirthDate: UITextField!
    @IBOutlet weak var txtName: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtName.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        txtName.resignFirstResponder()
    }
    
    func validate() -> Bool {
        
        if txtName.text == "" {
            self.view.makeToast(Constant.validationMessage.isValidName)
            return false
        }
        
        if txtBirthDate.text == "" {
            self.view.makeToast(Constant.validationMessage.isValidBirthDate)
            return false
        }
        
        return true
    }
    
    @IBAction func btnSelectDateAction(_ sender: UIButton) {
        
        txtName.resignFirstResponder()
        
        let alert = UIAlertController(title: "Select Birth Date", message: nil, preferredStyle: .actionSheet)
        
        let calendar = Calendar(identifier: .gregorian)
        
        let currentDate = Date()
        var components = DateComponents()
        components.calendar = calendar
        
        components.year = -17
        components.month = -12
        let maxDate = calendar.date(byAdding: components, to: currentDate)!
        
        alert.addDatePicker(mode: .date, date: Date(), minimumDate: nil, maximumDate: maxDate) { date in
            let dayDataformate = DateFormatter()
            dayDataformate.dateFormat = "d"
            
            let monthDataformate = DateFormatter()
            monthDataformate.dateFormat = "MM"
            
            let yearDataformate = DateFormatter()
            yearDataformate.dateFormat = "yyyy"
            
            let dataformate1 = DateFormatter()
            dataformate1.dateFormat = "MMMM d, yyyy"
            
            globalSignUpFinishParam[Constant.params.day] = dayDataformate.string(from: date)
            globalSignUpFinishParam[Constant.params.month] = monthDataformate.string(from: date)
            globalSignUpFinishParam[Constant.params.year] = yearDataformate.string(from: date)
            
            self.selectedDOB = dataformate1.string(from: date)
            self.txtBirthDate.text = dataformate1.string(from: date)
        }
        alert.addAction(UIAlertAction(title: "Done", style: .cancel, handler: nil))
        alert.show()
    }
    
    @IBAction func btnGoAction(_ sender: UIButton) {
        
        if validate() {
            globalSignUpFinishParam[Constant.params.name] = txtName.text
            
            GlobalFunction.showLoadingIndicator(color: 1)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.finishSignUpAPICall()
            }
        }
    }
    
    // MARK:- upload video story function call
    func finishSignUpAPICall() {
        
        var params: Parameters = [:]
        
        params[Constant.params.name]  = txtName.text
        params[Constant.params.sex]  = globalSignUpFinishParam[Constant.params.sex]
        params[Constant.params.interestedIn]  = globalSignUpFinishParam[Constant.params.interestedIn]
        params[Constant.params.year]  = globalSignUpFinishParam[Constant.params.year]
        params[Constant.params.month]  = globalSignUpFinishParam[Constant.params.month]
        params[Constant.params.day]  = globalSignUpFinishParam[Constant.params.day]
        params[Constant.params.question]  = globalSignUpFinishParam[Constant.params.question]
        params[Constant.params.location]  = "San Jose"
        
        var imageData : [[String:[Data]]] = []
        imageData = globalImageData
        
        WebAPIManager.makeMultipartRequestToUploadImages(method: .post, isFormDataRequest: true, isAuthenticationToken: true, isContainXAPIToken: false, isContainContentType: false, path: Constant.API.finishSignup, parameters: params as! [String : String], imagesData: imageData) { (response, status) in
            
            GlobalFunction.hideLoadingIndicator()
            
            let responseData = LoginModel(fromDictionary: response as! [String : Any])
            
            if responseData.status == 200 {
                APP_DELEGATE.window?.makeToast(responseData.message)
                responseData.saveInUserDefault()
                Constant.AppDel.currentLoginUser = responseData
                self.performSegue(withIdentifier: Constant.segueIdentifier.segueNameDateToHomeTab, sender: self)
                
            }else {
                APP_DELEGATE.window?.makeToast(responseData.message)
            }
        }
    }
}
