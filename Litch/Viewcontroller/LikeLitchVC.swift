//
//  LikeLitchVC.swift
//  Litch
//
//  Created by mac on 29/07/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import AlamofireImage
import AVKit
import AVFoundation

class LikeLitchVC: UIViewController,VerticalCardSwiperDelegate, VerticalCardSwiperDatasource {
    
    @IBOutlet weak var cardSwiper: VerticalCardSwiper!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblHowPeopleLike: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    
    var likeInfo : [GetLikesReceivedLike]?
    var tempLikeInfo : [GetLikesReceivedLike]?
    
    var totalCount = "0"
    var cardSwipedIn = 1
    
    @IBOutlet weak var lblNoCardMessage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cardSwiper.delegate = self
        cardSwiper.datasource = self
        cardSwiper.topInset = 0.0
        cardSwiper.cardSpacing = 0.0
        cardSwiper.sideInset = 20.0
        cardSwiper.visibleNextCardHeight = 0.0
        cardSwiper.firstItemTransform = 0.00
        cardSwiper.verticalCardSwiperView.isScrollEnabled = false
        cardSwiper.isStackOnBottom = true
        cardSwiper.isCardRemovalAllowed = true
        //        cardSwiper.isSideSwipingEnabled = true
        
        // register cardcell for storyboard use
        cardSwiper.register(nib: UINib(nibName: "ExampleCell", bundle: nil), forCellWithReuseIdentifier: "ExampleCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        tempLikeInfo = likeInfo
        lblNoCardMessage.isHidden = true
        cardSwiper.reloadData()
        
        lblHowPeopleLike.text = "\(cardSwipedIn) of \(tempLikeInfo?.count ?? 0) people who like you"
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        cardSwiper.layoutIfNeeded()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        
        litchAvPlayer?.seek(to: .zero)
        litchAvPlayer?.pause()
        litchAvPlayerLayer?.removeFromSuperlayer()
        litchAvPlayer = nil
        
        if let focussedCardIndex = cardSwiper.focussedCardIndex {
            let card = cardSwiper.verticalCardSwiperView.cellForItem(at: IndexPath(row: focussedCardIndex, section: 0)) as! ExampleCardCell
            card.imgLitch.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        }
    }
    
    @IBAction func btnLikeAction(_ sender: Any) {
        
        if let currentIndex = cardSwiper.focussedCardIndex {
            _ = cardSwiper.swipeCardAwayProgrammatically(at: currentIndex, to: .Right)
        }
    }
    
    @IBAction func btnDiscardAction(_ sender: Any) {
        
        if let currentIndex = cardSwiper.focussedCardIndex {
            _ = cardSwiper.swipeCardAwayProgrammatically(at: currentIndex, to: .Left)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBackGoAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfCards(verticalCardSwiperView: VerticalCardSwiperView) -> Int {
        return likeInfo?.count ?? 0
    }
    
    func cardForItemAt(verticalCardSwiperView: VerticalCardSwiperView, cardForItemAt index: Int) -> CardCell {
        if let cardCell = verticalCardSwiperView.dequeueReusableCell(withReuseIdentifier: "ExampleCell", for: index) as? ExampleCardCell {
            
            //             print("card for item at \(index)")
            let contact = likeInfo?[index]
            
            if contact?.litchImage == nil {
                cardCell.imgLitch.image = #imageLiteral(resourceName: "video_placeholder")
                cardCell.btnReplay.isHidden = true
            }else {
                cardCell.configureCell(imageUrl: contact?.litchImage, description: "Video", videoUrl: contact?.litch)
                
                DispatchQueue.main.async {
                    currentExampleCell = cardCell
                    currentVideoUrl = URL(string: contact?.litch ?? "")!
                    
                    cardCell.imgLitch.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                    
                    self.PlayVideoVideoCell(cell: cardCell, url: URL(string: contact?.litch ?? "")!)
                    cardCell.btnReplay.isHidden = true
                }
            }
            
            if likeInfo?.count != 0 {
                DispatchQueue.main.async {
                    if contact?.litch != nil {
                        self.lblQuestion.text = contact?.litchQuestion
                        self.lblAge.text = "\(contact?.age ?? 0)"
                        self.lblUserName.text = contact?.name
                    }
                    
                }
            }
            
            cardCell.btnReplayCompletion = {
                self.restartVideoVideoCell(cell: cardCell, url: URL(string: contact?.litch ?? "")!)
            }
            return cardCell
        }
        return CardCell()
    }
    
    
    func restartVideoVideoCell(cell: ExampleCardCell,url: URL) {
        
        litchAvPlayerLayer?.opacity = 1
        litchAvPlayer?.seek(to: CMTime.zero)
        //        litchAvPlayer?.playImmediately(atRate: 1.0)
        litchAvPlayer?.play()
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            cell.btnReplay.isHidden = true
            cell.imgBlur.isHidden = true
        }
    }
    
    func PlayVideoVideoCell(cell: ExampleCardCell,url: URL) {
        
        let item = AVPlayerItem(url: url)
        litchAvPlayer = AVPlayer(playerItem: item)
        litchAvPlayer?.actionAtItemEnd = .none
        litchAvPlayerLayer = AVPlayerLayer(player: litchAvPlayer)
        
        litchAvPlayerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        litchAvPlayerLayer?.frame = cell.imgLitch.bounds
        
        cell.imgLitch.layer.addSublayer(litchAvPlayerLayer!)
        cell.imgLitch.setNeedsUpdateConstraints()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerItemDidReachEnd), name: .AVPlayerItemDidPlayToEndTime, object: litchAvPlayer?.currentItem!)
        litchAvPlayer?.play()
    }
    
    @objc func playerItemDidReachEnd() {
        
        DispatchQueue.main.async {
            litchAvPlayerLayer?.opacity = 0
            currentExampleCell?.btnReplay.isHidden = false
            currentExampleCell?.imgBlur.isHidden = false
        }
    }
    
    func willSwipeCardAway(card: CardCell, index: Int, swipeDirection: SwipeDirection) {
        
        if index < likeInfo?.count ?? 0 {
            
            if swipeDirection == .Left {
                var emptyDictionary = [String: String]()
                emptyDictionary["discardedUsersIds[0]"] = likeInfo?[index].userId
                
                DiscardPost(value: emptyDictionary)
            }else if swipeDirection == .Right {
                
                LikePost(id: likeInfo?[index].userId ?? "")
            }
            
            let card = cardSwiper.verticalCardSwiperView.cellForItem(at: IndexPath(row: index, section: 0)) as! ExampleCardCell
            
            DispatchQueue.main.async {
                card.imgLitch.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
            }
            
            likeInfo?.remove(at: index)
            
            if likeInfo?.count == 0 {
                lblQuestion.text = ""
                lblAge.text = ""
                lblUserName.text = ""
            }
            
            lblHowPeopleLike.text = "\(cardSwipedIn + 1) of \(tempLikeInfo?.count ?? 0) people who like you"
            cardSwipedIn = cardSwipedIn + 1
            
            if likeInfo?.count == 0 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    func didSwipeCardAway(card: CardCell, index: Int, swipeDirection: SwipeDirection) {
    }
    
    // MARK:- like API method call
    private func LikePost(id: String) {
        var params: Parameters = [:]
        params[Constant.params.likedUsersId]  = id
        
        WebAPIManager.makeAPIRequest(isFormDataRequest: true,isAuthenticationToken: true, isContainXAPIToken: false, isContainContentType: true, path:Constant.API.like, params: params) { (response, status) in
            
            GlobalFunction.hideLoadingIndicator()
            print("response is \(response)")
            let responseData = LikeModel(fromDictionary: response as! [String : Any])
            
            if responseData.status == 200 {
                
            }else {
                self.view.makeToast(responseData.message)
            }
        }
    }
    
    // MARK:- like API method call
    private func DiscardPost(value: [String:String]) {
        
        var params: Parameters = [:]
        params = value
        
        WebAPIManager.makeAPIRequest(isFormDataRequest: true,isAuthenticationToken: true, isContainXAPIToken: false, isContainContentType: false, path:Constant.API.discard, params: params) { (response, status) in
            
            let responseData = DiscardModel(fromDictionary: response as! [String : Any])
            
            if responseData.status == 200 {
            }
        }
    }
}
