//
//  FavoriteVC.swift
//  Litch
//
//  Created by Kishan Suthar on 30/06/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class FavoriteVC: UIViewController {
    
    @IBOutlet weak var tblMatches: UITableView!
    @IBOutlet weak var cvLike: UICollectionView!
    
    var getLikes = [GetLikesReceivedLike]()
    var getMatches = [GetMatchesMatche]()
    
    var selectedMatch : GetMatchesMatche?
    var selectedLikes : [GetLikesReceivedLike]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        getLikes.removeAll()
        getMatches.removeAll()
        
        tblMatches.reloadData()
        cvLike.reloadData()
        
        GlobalFunction.showLoadingIndicator(color: 1)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.getLikesAPI()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK:- like API method call
    private func getLikesAPI() {
        
        WebAPIManager.makeAPIRequest(isFormDataRequest: true,isAuthenticationToken: true, isContainXAPIToken: false, isContainContentType: true, path:Constant.API.getLikes, params: [:]) { (response, status) in
            
            self.getLikes.removeAll()
            
            let responseData = GetLikesModel(fromDictionary: response as! [String : Any])
            
            if responseData.status == 200 {
                self.getLikes = responseData.receivedLikes
                
                self.getLikes.count == 0 ? GlobalFunction.addInLineView(in: self.cvLike, title: "Well this is awkward...") : GlobalFunction.removeInLineView()
                self.cvLike.reloadData()
            }else {
                APP_DELEGATE.window?.makeToast(responseData.message)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.getMatchesAPI()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constant.segueIdentifier.segueFavoriteToChat {
            let vc = segue.destination as! ChatVC
            vc.matchInfo = selectedMatch
        }else if segue.identifier == Constant.segueIdentifier.segueFavoriteToLikeLitch {
            let vc = segue.destination as! LikeLitchVC
            vc.likeInfo = selectedLikes
            vc.totalCount = getLikes.count.description
        }
    }
    
    // MARK:- like API method call
    private func getMatchesAPI() {
        
        WebAPIManager.makeAPIRequest(isFormDataRequest: true,isAuthenticationToken: true, isContainXAPIToken: false, isContainContentType: true, path:Constant.API.getMatches, params: [:]) { (response, status) in
            
            self.getMatches.removeAll()
            let responseData = GetMatchesModel(fromDictionary: response as! [String : Any])
            
            if responseData.status == 200 {
                self.getMatches = responseData.matches
                
                self.getMatches.count == 0 ? GlobalFunction.addMatchView(in: self.tblMatches, title: "It's not you, it's them...") : GlobalFunction.removeMatchView()
                self.tblMatches.reloadData()
            }else {
                APP_DELEGATE.window?.makeToast(responseData.message)
            }
        }
    }
    
}
extension FavoriteVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return getLikes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = cvLike.dequeueReusableCell(withReuseIdentifier: "FavoriteLikeCVC", for: indexPath) as! FavoriteLikeCVC
        
        if getLikes[indexPath.row].litchImage != nil {
            cell.imgProfile.sd_setImage(with: URL(string: getLikes[indexPath.row].litchImage), placeholderImage: #imageLiteral(resourceName: "img_placeholder"), options: .continueInBackground, context: nil)
        }else {
            cell.imgProfile.sd_setImage(with: URL(string: ""), placeholderImage: #imageLiteral(resourceName: "img_placeholder"), options: .continueInBackground, context: nil)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90.0, height: 90.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var temp = [GetLikesReceivedLike]()
        for i in indexPath.row..<getLikes.count {
            temp.append(getLikes[i])
        }
        
        selectedLikes = temp
        self.performSegue(withIdentifier: Constant.segueIdentifier.segueFavoriteToLikeLitch, sender: self)
    }
}

extension FavoriteVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getMatches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblMatches.dequeueReusableCell(withIdentifier: "FavoriteMatchTVC", for: indexPath) as! FavoriteMatchTVC
        if getMatches[indexPath.row].person.litchImage != nil {
            cell.imgProfile.sd_setImage(with: URL(string: getMatches[indexPath.row].person.litchImage), placeholderImage: #imageLiteral(resourceName: "img_placeholder"), options: .continueInBackground, context: nil)
        }
        cell.lblName.text = getMatches[indexPath.row].person.name
        if getMatches[indexPath.row].lastChatMessage == "" {
            cell.lblMessage.text = "Start chatting now"
        }else {
            cell.lblMessage.text = getMatches[indexPath.row].lastChatMessage
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedMatch = getMatches[indexPath.row]
        self.performSegue(withIdentifier: Constant.segueIdentifier.segueFavoriteToChat, sender: self)
    }
    
}
