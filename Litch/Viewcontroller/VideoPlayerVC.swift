//
//  VideoPlayerVC.swift
//  Litch
//
//  Created by Kishan Suthar on 06/07/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

var globalImageData : [[String:[Data]]] = []

class VideoPlayerVC: UIViewController {
    
    @IBOutlet weak var videoView: UIView!
    
    @IBOutlet weak var btnPlayPause: UIButton!
    
    var playerUrl: URL!
    
    var player: AVPlayer!
    var avpController = AVPlayerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        globalImageData.removeAll()
        player = AVPlayer(url: playerUrl)
        avpController.player = player
        avpController.view.frame.size.height = videoView.frame.size.height
        avpController.view.frame.size.width = videoView.frame.size.width
        avpController.entersFullScreenWhenPlaybackBegins = true
        avpController.showsPlaybackControls = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying(note:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        self.videoView.addSubview(avpController.view)
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification) {
        //        print("Video Finished")
        btnPlayPause.isSelected = false
        avpController.player?.seek(to: .zero)
    }
    
    @IBAction func btnPlayPauseAction(_ sender: UIButton) {
        
        if sender.isSelected {
            sender.isSelected = false
            //            print("Video pause")
            avpController.player?.pause()
        }else {
            sender.isSelected = true
            //            print("Video start")
            avpController.player?.play()
        }
    }
    
    @IBAction func btnRetakeAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUseVideoAction(_ sender: UIButton) {
        
        getThumbnailImageFromVideoUrl(url: playerUrl) { (image) in
            
            do {
                let videoData = try Data(contentsOf: self.playerUrl)
                let imgData = image?.jpegData(compressionQuality: 0.5)
                globalImageData.append(["video": [videoData]])
                globalImageData.append(["image": [imgData!]])
                
                globalSignUpFinishParam[Constant.params.video] = self.playerUrl.absoluteString
                self.performSegue(withIdentifier: Constant.segueIdentifier.segueVideoPlayerToGenderInterest, sender: self)
            }catch{
            }
        }
    }
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
}
