
//
//  HomeVideoCardVC.swift
//  Litch
//
//  Created by mac on 07/07/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit
import TinderSwipeView
import SDWebImage
import Alamofire
import AlamofireImage
import AVKit
import AVFoundation

var litchAvPlayer : AVPlayer?
var litchAvPlayerLayer : AVPlayerLayer?

var currentExampleCell : ExampleCardCell?
var currentVideoUrl : URL?

class HomeVideoCardVC: UIViewController,VerticalCardSwiperDelegate, VerticalCardSwiperDatasource {
    
    @IBOutlet weak var cardSwiper: VerticalCardSwiper!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserAge: UILabel!
    
    var browseLitch : [BrowseLocalUser] = []
    var tempBrowseLitch : [BrowseLocalUser] = []
    
//    var emptyDictionary = [String: String]()
//    var discardUserCount = 0
    
    @IBOutlet weak var lblNoCardMessage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cardSwiper.delegate = self
        cardSwiper.datasource = self
        cardSwiper.topInset = 0.0
        cardSwiper.cardSpacing = 0.0
        cardSwiper.sideInset = 20.0
        cardSwiper.visibleNextCardHeight = 0.0
        cardSwiper.firstItemTransform = 0.00
        cardSwiper.stackedCardsCount = 2
        cardSwiper.verticalCardSwiperView.isScrollEnabled = false
        cardSwiper.isStackOnBottom = true
        cardSwiper.isCardRemovalAllowed = true
        
        // register cardcell for storyboard use
        cardSwiper.register(nib: UINib(nibName: "ExampleCell", bundle: nil), forCellWithReuseIdentifier: "ExampleCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        lblNoCardMessage.isHidden = true
        cardSwiper.reloadData()
        GlobalFunction.showLoadingIndicator(color: 1)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.BrowseLitch()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        cardSwiper.layoutIfNeeded()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        litchAvPlayer?.seek(to: .zero)
        litchAvPlayer?.pause()
        litchAvPlayerLayer?.removeFromSuperlayer()
        litchAvPlayer = nil
        
        if let focussedCardIndex = cardSwiper.focussedCardIndex {
            let card = cardSwiper.verticalCardSwiperView.cellForItem(at: IndexPath(row: focussedCardIndex, section: 0)) as! ExampleCardCell
            card.imgLitch.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        }
    }
    
    @IBAction func btnDislikeLitchAction(_ sender: UIButton) {
        
        if let currentIndex = cardSwiper.focussedCardIndex {
            _ = cardSwiper.swipeCardAwayProgrammatically(at: currentIndex, to: .Left)
        }
    }
    
    @IBAction func btnLikeLitchAction(_ sender: UIButton) {
        if let currentIndex = cardSwiper.focussedCardIndex {
            _ = cardSwiper.swipeCardAwayProgrammatically(at: currentIndex, to: .Right)
        }
    }
    
    // MARK:- like API method call
       private func flagUser(id: String) {
           var params: Parameters = [:]
           params[Constant.params.flaggedUserId]  = id
           
           WebAPIManager.makeAPIRequest(isFormDataRequest: true,isAuthenticationToken: true, isContainXAPIToken: false, isContainContentType: true, path:Constant.API.flagUser, params: params) { (response, status) in
               
               GlobalFunction.hideLoadingIndicator()
               let responseData = FlagUserModel(fromDictionary: response as! [String : Any])
               
               if responseData.status == 200 {
                if let currentIndex = self.cardSwiper.focussedCardIndex {
                    _ = self.cardSwiper.swipeCardAwayProgrammatically(at: currentIndex, to: .Left)
                }
               }else {
                   APP_DELEGATE.window?.makeToast(responseData.message)
               }
           }
       }
    
    
    // MARK:- like API method call
    private func LikePost(id: String) {
        var params: Parameters = [:]
        params[Constant.params.likedUsersId]  = id
        
        WebAPIManager.makeAPIRequest(isFormDataRequest: true,isAuthenticationToken: true, isContainXAPIToken: false, isContainContentType: true, path:Constant.API.like, params: params) { (response, status) in
            
            let responseData = LikeModel(fromDictionary: response as! [String : Any])
            
            if responseData.status == 200 {
                
            }else {
                APP_DELEGATE.window?.makeToast(responseData.message)
            }
        }
    }
    
    // MARK:- like API method call
    private func DiscardPost(value: [String:String]) {
        
        var params: Parameters = [:]
        params = value
        
        WebAPIManager.makeAPIRequest(isFormDataRequest: true,isAuthenticationToken: true, isContainXAPIToken: false, isContainContentType: false, path:Constant.API.discard, params: params) { (response, status) in
            
            let responseData = DiscardModel(fromDictionary: response as! [String : Any])
            
            if responseData.status == 200 {
                
            }
        }
    }
    
    // MARK:- Browse Litch API method call
    private func BrowseLitch() {
        
        WebAPIManager.makeAPIRequest(isFormDataRequest: true,isAuthenticationToken: true, isContainXAPIToken: false, isContainContentType: false, path:Constant.API.browseLitch, params: [:]) { (response, status) in
            
            GlobalFunction.hideLoadingIndicator()
            let responseData = BrowseModel(fromDictionary: response as! [String : Any])
            
            if responseData.status == 200 {
                APP_DELEGATE.window?.makeToast(responseData.message)
                
                self.browseLitch = responseData.localUsers
                self.tempBrowseLitch = self.browseLitch
                self.cardSwiper.reloadData()
                
                if self.tempBrowseLitch.count == 0 {
                    self.lblNoCardMessage.text = "Well... you have gone through all of \(currentAccountInfo.location ?? "")."
                    self.lblNoCardMessage.isHidden = false
                }
                
            }else {
                APP_DELEGATE.window?.makeToast(responseData.message)
            }
        }
    }
    
    func numberOfCards(verticalCardSwiperView: VerticalCardSwiperView) -> Int {
        return tempBrowseLitch.count
    }
    
    func cardForItemAt(verticalCardSwiperView: VerticalCardSwiperView, cardForItemAt index: Int) -> CardCell {
        if let cardCell = verticalCardSwiperView.dequeueReusableCell(withReuseIdentifier: "ExampleCell", for: index) as? ExampleCardCell {
            
            let contact = tempBrowseLitch[index]
            
            if contact.litch != nil {
                if contact.litch.imageFile == nil {
                    DispatchQueue.main.async {
                        cardCell.imgLitch.image = #imageLiteral(resourceName: "video_placeholder")
                        cardCell.btnReplay.isHidden = true
                    }
                }else {
                    cardCell.configureCell(imageUrl: contact.litch.imageFile, description: "Video", videoUrl: contact.litch.videoFile)
                    DispatchQueue.main.async {
                        currentExampleCell = cardCell
                        currentVideoUrl = URL(string: contact.litch.videoFile)!
                        
                        cardCell.imgLitch.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                        
                        self.PlayVideoVideoCell(cell: cardCell, url: URL(string: contact.litch.videoFile)!)
                        
                        cardCell.btnReplay.isHidden = true
                    }
                }
            }
            
            if browseLitch.count != 0 {
                DispatchQueue.main.async {
                    if contact.litch != nil {
                        self.lblQuestion.text = contact.litch.question
                        self.lblUserAge.text = "\(contact.age ?? 0)"
                        self.lblUserName.text = contact.litch.author.name
                    }
                    
                }
            }
            
            cardCell.btnReplayCompletion = {
                DispatchQueue.main.async {
                    //                    cardCell.restartVideo()
                    self.restartVideoVideoCell(cell: cardCell, url: URL(string: contact.litch.videoFile)!)
                }
            }
            
            cardCell.btnMoreCompletion = {
                
                let optionMenu = UIAlertController(title: nil, message: "Is this video inappropriate?", preferredStyle: .actionSheet)
                
                let deleteAction = UIAlertAction(title: "Flag this user", style: .destructive) { (status) in
                    GlobalFunction.showLoadingIndicator(color: 1)
                    DispatchQueue.main.async {
                        self.flagUser(id: contact.litch.author.id)
                    }
                }

                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                optionMenu.addAction(deleteAction)
                optionMenu.addAction(cancelAction)
                self.present(optionMenu, animated: true, completion: nil)
            }
            return cardCell
        }
        return CardCell()
    }
    
    func restartVideoVideoCell(cell: ExampleCardCell,url: URL) {
        
        DispatchQueue.main.async {
            cell.btnReplay.isHidden = true
            cell.imgBlur.isHidden = true
        }
        
        litchAvPlayer?.playImmediately(atRate: 1.0)
        litchAvPlayerLayer?.opacity = 1
        
        litchAvPlayer?.seek(to: CMTime.zero)
        litchAvPlayer?.play()
    }
    
    func PlayVideoVideoCell(cell: ExampleCardCell,url: URL) {
        
        let item = AVPlayerItem(url: url)
        litchAvPlayer = AVPlayer(playerItem: item)
        litchAvPlayer?.actionAtItemEnd = .none
        litchAvPlayerLayer = AVPlayerLayer(player: litchAvPlayer)
        
        litchAvPlayerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        litchAvPlayerLayer?.frame = cell.imgLitch.bounds
        
        cell.imgLitch.layer.addSublayer(litchAvPlayerLayer!)
        cell.imgLitch.setNeedsUpdateConstraints()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerItemDidReachEnd), name: .AVPlayerItemDidPlayToEndTime, object: litchAvPlayer?.currentItem!)
        litchAvPlayer?.play()
    }
    
    @objc func playerItemDidReachEnd() {
        
        DispatchQueue.main.async {
            litchAvPlayerLayer?.opacity = 0
            currentExampleCell?.btnReplay.isHidden = false
            currentExampleCell?.imgBlur.isHidden = false
        }
    }
    
    func willSwipeCardAway(card: CardCell, index: Int, swipeDirection: SwipeDirection) {
        
        if index < tempBrowseLitch.count {
            
            if swipeDirection == .Left {
                
                if self.tempBrowseLitch[index].litch != nil {
                  
                    var emptyDictionary = [String: String]()
                    emptyDictionary["discardedUsersIds[0]"] = self.tempBrowseLitch[index].litch.author.id
                    self.DiscardPost(value: emptyDictionary)
                    
//                    emptyDictionary["discardedUsersIds[\(discardUserCount)]"] = self.tempBrowseLitch[index].litch.author.id
//
//                    //                print(emptyDictionary)
//                    discardUserCount = discardUserCount + 1
//
//                    if discardUserCount == 3 {
//
//                        DispatchQueue.global().asyncAfter(deadline: .now() + 0.1) {
//                            self.DiscardPost(value: self.emptyDictionary)
//                            self.emptyDictionary.removeAll()
//                            self.discardUserCount = 0
//                        }
//                    }else if tempBrowseLitch.count == 1 {
//                        if emptyDictionary.count > 0 {
//                            //                        print(emptyDictionary)
//                            DispatchQueue.global().asyncAfter(deadline: .now() + 0.1) {
//                                self.DiscardPost(value: self.emptyDictionary)
//                                self.emptyDictionary.removeAll()
//                            }
//                        }
//                    }
                    
                }
                
                
            }else if swipeDirection == .Right {
                if self.tempBrowseLitch[index].litch != nil {
                    self.LikePost(id: self.tempBrowseLitch[index].litch.author.id)
                }
            }
            
            let card = cardSwiper.verticalCardSwiperView.cellForItem(at: IndexPath(row: index, section: 0)) as! ExampleCardCell
            
            DispatchQueue.main.async {
                card.imgLitch.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
            }
            
            tempBrowseLitch.remove(at: index)
            
            if tempBrowseLitch.count == 0 {
                lblQuestion.text = ""
                lblUserAge.text = ""
                lblUserName.text = ""
            }
            
            if tempBrowseLitch.count == 0 {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    card.imgLitch.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                    
                    self.lblNoCardMessage.text = "Well... you have gone through all of \(currentAccountInfo.location ?? "")."
                    self.lblNoCardMessage.isHidden = false
                }
            }
        }
    }
    
    func didSwipeCardAway(card: CardCell, index: Int, swipeDirection: SwipeDirection) {
    }
}
