//
//  MyLitchVC.swift
//  Litch
//
//  Created by mac on 29/07/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import AlamofireImage
import AVKit
import AVFoundation

class MyLitchVC: UIViewController,VerticalCardSwiperDelegate, VerticalCardSwiperDatasource  {
    
    @IBOutlet weak var cardSwiper: VerticalCardSwiper!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserAge: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cardSwiper.delegate = self
        cardSwiper.datasource = self
        cardSwiper.topInset = 0.0
        cardSwiper.cardSpacing = 0.0
        cardSwiper.sideInset = 20.0
        cardSwiper.visibleNextCardHeight = 0.0
        cardSwiper.firstItemTransform = 0.00
        cardSwiper.verticalCardSwiperView.isScrollEnabled = false
        cardSwiper.isStackOnBottom = true
        cardSwiper.isSideSwipingEnabled = false
        // register cardcell for storyboard use
        
        cardSwiper.layer.cornerRadius = 20.0
        cardSwiper.register(nib: UINib(nibName: "ExampleCell", bundle: nil), forCellWithReuseIdentifier: "ExampleCell")
        cardSwiper.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        cardSwiper.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        cardSwiper.layoutIfNeeded()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        litchAvPlayer?.seek(to: .zero)
        litchAvPlayer?.pause()
        litchAvPlayerLayer?.removeFromSuperlayer()
        litchAvPlayer = nil
        
        if let focussedCardIndex = cardSwiper.focussedCardIndex {
            let card = cardSwiper.verticalCardSwiperView.cellForItem(at: IndexPath(row: focussedCardIndex, section: 0)) as! ExampleCardCell
            card.imgLitch.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        }
    }
    
    @IBAction func btnBackGoAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfCards(verticalCardSwiperView: VerticalCardSwiperView) -> Int {
        return 1
    }
    
    func cardForItemAt(verticalCardSwiperView: VerticalCardSwiperView, cardForItemAt index: Int) -> CardCell {
        if let cardCell = verticalCardSwiperView.dequeueReusableCell(withReuseIdentifier: "ExampleCell", for: index) as? ExampleCardCell {
            
            let contact = currentAccountInfo.litch
            
            if contact?.imageFile == nil {
                cardCell.imgLitch.image = #imageLiteral(resourceName: "video_placeholder")
                cardCell.btnReplay.isHidden = true
            }else {
                cardCell.configureCell(imageUrl: contact?.imageFile, description: "Video", videoUrl: contact?.videoFile)
                
                DispatchQueue.main.async {
                    currentExampleCell = cardCell
                    currentVideoUrl = URL(string: contact?.videoFile ?? "")!
                    
                    cardCell.imgLitch.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                    
                    self.PlayVideoVideoCell(cell: cardCell, url: URL(string: contact?.videoFile ?? "")!)
                    //                    cardCell.setUpVideoPlayer(url: URL(string: contact.litch.videoFile)!)
                    cardCell.btnReplay.isHidden = true
                }
            }
            
            lblQuestion.text = contact?.question
            lblUserAge.text = "\(currentAccountInfo.age ?? 0)"
            lblUserName.text = currentAccountInfo.name
            
            cardCell.btnReplayCompletion = {
                self.restartVideoVideoCell(cell: cardCell, url: URL(string: contact?.videoFile ?? "")!)
            }
            return cardCell
        }
        return CardCell()
    }
    
    func restartVideoVideoCell(cell: ExampleCardCell,url: URL) {
        
        DispatchQueue.main.async {
            cell.btnReplay.isHidden = true
            cell.imgBlur.isHidden = true
            litchAvPlayerLayer?.opacity = 1
            
            //            litchAvPlayer?.playImmediately(atRate: 1.0)
            litchAvPlayer?.seek(to: CMTime.zero)
            litchAvPlayer?.play()
        }
    }
    
    func PlayVideoVideoCell(cell: ExampleCardCell,url: URL) {
        
        let item = AVPlayerItem(url: url)
        litchAvPlayer = AVPlayer(playerItem: item)
        litchAvPlayer?.actionAtItemEnd = .none
        litchAvPlayerLayer = AVPlayerLayer(player: litchAvPlayer)
        
        litchAvPlayerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        litchAvPlayerLayer?.frame = cell.imgLitch.bounds
        
        cell.imgLitch.layer.addSublayer(litchAvPlayerLayer!)
        cell.imgLitch.setNeedsUpdateConstraints()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerItemDidReachEnd), name: .AVPlayerItemDidPlayToEndTime, object: litchAvPlayer?.currentItem!)
        litchAvPlayer?.play()
    }
    
    @objc func playerItemDidReachEnd() {
        
        DispatchQueue.main.async {
            litchAvPlayerLayer?.opacity = 0
            currentExampleCell?.btnReplay.isHidden = false
            currentExampleCell?.imgBlur.isHidden = false
        }
    }
    
    func willSwipeCardAway(card: CardCell, index: Int, swipeDirection: SwipeDirection) {
    }
    
    func didSwipeCardAway(card: CardCell, index: Int, swipeDirection: SwipeDirection) {
    }
}
