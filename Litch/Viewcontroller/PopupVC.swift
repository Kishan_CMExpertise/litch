//
//  PopupVC.swift
//  Litch
//
//  Created by mac on 18/07/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit
import StoreKit
import Alamofire

class PopupVC: UIViewController {
    
    var litchDoubleTimeCompletion: ((Bool)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideScreen(_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideScreen (_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDoubleMyTimeAction(_ sender: Any) {
        
        StoreManager.shared.delegate = self
        GlobalFunction.showLoadingIndicator(color: 1)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            StoreManager.shared.buyAccessCode()
        })
    }
    
    @IBAction func btnTermsConditionAction(_ sender: UIButton) {
        
        let url = URL(string: "https://litch.herokuapp.com/terms")!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            
            UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                //                 print("Open url : \(success)")
            })
        }
    }
    
    @IBAction func btnRestorePurchaseAction(_ sender: UIButton) {
        //        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK:- like API method call
    private func createPurchase() {
        
        WebAPIManager.makeAPIRequest(isFormDataRequest: true,isAuthenticationToken: true, isContainXAPIToken: false, isContainContentType: true, path:Constant.API.createPurchase, params: [:]) { (response, status) in
            
            GlobalFunction.hideLoadingIndicator()
            let responseData =  response as! [String : Any]
            
            let status = responseData["status"] as! Int
            let message = responseData["message"] as! String
            
            if status == 200 {
                self.litchDoubleTimeCompletion?(true)
                UIApplication.shared.windows.first?.makeToast(message)
                self.dismiss(animated: true, completion: nil)
            }else {
                UIApplication.shared.windows.first?.makeToast(message)
            }
        }
    }
}


extension PopupVC: StoreManagerProtocol {
    
    func didPurchase(purchaseType: String, transaction: SKPaymentTransaction) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.receiptValidation()
        }
    }
    
    func receiptValidation() {
        let strLive = "https://buy.itunes.apple.com/verifyReceipt"
        let strSendBox = "https://sandbox.itunes.apple.com/verifyReceipt"
        
        let urlLive = URL(string: strLive)!
        let urlSendBox = URL(string: strSendBox)!
        
        if let receiptUrl = Bundle.main.appStoreReceiptURL {
            if let receipt = try? Data.init(contentsOf: receiptUrl) {
                var params: [String: String] = [:]
                let rData = receipt.base64EncodedString(options: Data.Base64EncodingOptions.init(rawValue: 0))
                
                params["receipt-data"] = rData
                
                var urlRequest = URLRequest(url: urlLive)
                urlRequest.httpMethod = "POST"
                
                do {
                    urlRequest.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                }
                
                self.makeURLRequestToApple(urlRequest: urlRequest) { (response) in
                    let status = response["status"] as? Int
                    if status == 21007 {
                        urlRequest.url = urlSendBox
                        self.makeURLRequestToApple(urlRequest: urlRequest) { (resNew) in
                            let statusNew = resNew["status"] as? Int
                            if statusNew == 21007 {
                                GlobalFunction.hideLoadingIndicator()
                                
                                GlobalFunction.showLoadingIndicator(color: 1)
                                DispatchQueue.main.async {
                                    self.createPurchase()
                                    //                                    self.litchDoubleTimeCompletion?(true)
                                    //                                self.dismiss(animated: true, completion: nil)
                                }
                            } else if statusNew == 0 {
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                    GlobalFunction.hideLoadingIndicator()
                                    GlobalFunction.showLoadingIndicator(color: 1)
                                    DispatchQueue.main.async {
                                        self.createPurchase()
                                        //                                    self.litchDoubleTimeCompletion?(true)
                                        //                                self.dismiss(animated: true, completion: nil)
                                    }
                                    
                                }
                            } else {
                                GlobalFunction.hideLoadingIndicator()
                            }
                        }
                    } else if status == 0 {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            GlobalFunction.hideLoadingIndicator()
                            GlobalFunction.showLoadingIndicator(color: 1)
                            DispatchQueue.main.async {
                                self.createPurchase()
                                //                                    self.litchDoubleTimeCompletion?(true)
                                //                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    } else {
                        GlobalFunction.hideLoadingIndicator()
                    }
                }
            }
        }
    }
    
    func makeURLRequestToApple(urlRequest: URLRequest,  completion: @escaping (_ response: [String: Any]) -> Void) {
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                completion([:])
                return
            }
            
            guard let data = data else {
                completion([:])
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                    completion(json)
                }
            } catch let error {
                completion([:])
            }
        })
        task.resume()
    }
    
    func didError(_ error: Error) {
        DispatchQueue.main.async {
            GlobalFunction.hideLoadingIndicator()
        }
    }
}
