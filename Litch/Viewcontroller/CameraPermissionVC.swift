//
//  CameraPermissionVC.swift
//  Litch
//
//  Created by Kishan Suthar on 14/08/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class CameraPermissionVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            
            AVCaptureDevice.requestAccess(for: .audio, completionHandler: { (granted: Bool) in
                if granted {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.presentCameraSettings()
                    }
                }
            })
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    
                    AVCaptureDevice.requestAccess(for: .audio, completionHandler: { (granted: Bool) in
                        if granted {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                self.navigationController?.popViewController(animated: true)
                            }
                        } else {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                self.presentCameraSettings()
                            }
                        }
                    })
                } else {
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.presentCameraSettings()
                    }
                }
            })
        }
        
        
        //        self.navigationController?.popViewController(animated: true)
    }
    
    func presentCameraSettings() {
        
        if let url = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        
        //        let alertController = UIAlertController(title: "Access denied",
        //                                                message: "You can enable access to camera in privacy settings",
        //                                                preferredStyle: .alert)
        //        alertController.addAction(UIAlertAction(title: "Close", style: .default))
        //        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
        //            if let url = URL(string: UIApplication.openSettingsURLString) {
        //                UIApplication.shared.open(url, options: [:], completionHandler: nil)
        //            }
        //        })
        //
        //        present(alertController, animated: true)
    }
    
}
