//
//  HomeTabVC.swift
//  Litch
//
//  Created by mac on 20/07/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit

class HomeTabVC: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserInfoManager.getUserInfoModel() != nil {
            currentAccountInfo =  UserInfoManager.getUserInfoModel()
        }
        
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 0.1) {
            self.getUserData()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK:- get User API method call
    private func getUserData() {
        
        WebAPIManager.makeAPIRequest(isFormDataRequest: true,isAuthenticationToken: true, isContainXAPIToken: false, isContainContentType: true, path:Constant.API.getAccountInfo, params: [:]) { (response, status) in
            
            let responseData =  AccountInfoModel(fromDictionary: response as! [String : Any])
            if responseData.status == 200 {
                UserInfoManager.setUserInfo(userInfoModel: responseData)
                currentAccountInfo = UserInfoManager.getUserInfoModel()
            }
        }
    }
}
