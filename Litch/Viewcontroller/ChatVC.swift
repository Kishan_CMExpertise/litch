//
//  ChatVC.swift
//  Litch
//
//  Created by mac on 27/07/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import IQKeyboardManagerSwift

class ChatVC: UIViewController {
    
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var tblChat: UITableView!
    var matchInfo : GetMatchesMatche?
    
    @IBOutlet weak var txtMessage: UITextField!
    
    @IBOutlet weak var messageViewBottomConstraint: NSLayoutConstraint!
    
    
    var chatHistory = [ChatMessageChatHistory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblChat.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
        
        IQKeyboardManager.shared.enable = false
        lblUserName.text = matchInfo?.person.name
        imgUserProfile.sd_setImage(with: URL(string: matchInfo?.person.litchImage ?? ""), placeholderImage: #imageLiteral(resourceName: "img_placeholder"), options: SDWebImageOptions.continueInBackground, context: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.getChatHistoryAPI()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        IQKeyboardManager.shared.enable = true
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            UIView.animate(withDuration: 0.1) {
                self.messageViewBottomConstraint.constant = -keyboardHeight
            }
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            UIView.animate(withDuration: 0.1) {
                self.messageViewBottomConstraint.constant = 0
            }
            
        }
    }
    
    
    @IBAction func btnSendMessageAction(_ sender: UIButton) {
        
        if txtMessage.isEmpty() {
            APP_DELEGATE.window?.makeToast("Please write something")
        }else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.sendMessageAPI(msg: self.txtMessage.text ?? "")
            }
        }
    }
    
    @IBAction func btnBackGoAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- like API method call
    private func sendMessageAPI(msg: String) {
        
        var params: Parameters = [:]
        params[Constant.params.matchId]  = matchInfo?.matchId
        params[Constant.params.messageText] = msg
        
        WebAPIManager.makeAPIRequest(isFormDataRequest: true,isAuthenticationToken: true, isContainXAPIToken: false, isContainContentType: true, path:Constant.API.createChatMessage, params: params) { (response, status) in
            
            self.txtMessage.resignFirstResponder()
            self.txtMessage.text = ""
            let responseData = ChatMessageModel(fromDictionary: response as! [String : Any])
            
            if responseData.status == 200 {
                self.chatHistory = responseData.chatHistory
                self.tblChat.reloadData()
                if self.chatHistory.count > 0 {
                    
                    self.tblChat.scrollToRow(at: IndexPath(row: self.chatHistory.count - 1, section: 0), at: .bottom, animated: true)
                }
            }else {
            }
        }
    }
    
    // MARK:- like API method call
    private func getChatHistoryAPI() {
        
        var params: Parameters = [:]
        params[Constant.params.matchId]  = matchInfo?.matchId
        
        WebAPIManager.makeAPIRequest(isFormDataRequest: true,isAuthenticationToken: true, isContainXAPIToken: false, isContainContentType: true, path:Constant.API.getChatHistory, params: params) { (response, status) in
            
            let responseData = ChatMessageModel(fromDictionary: response as! [String : Any])
            
            if responseData.status == 200 {
                self.chatHistory = responseData.chatHistory
                self.tblChat.reloadData()
                if self.chatHistory.count > 0 {
                    self.tblChat.scrollToRow(at: IndexPath(row: self.chatHistory.count - 1, section: 0), at: .bottom, animated: true)
                }
                //
                //                //                  self.getLikes.count == 0 ? GlobalFunction.addInLineView(in: self.cvLike, title: "No one like yet!") : GlobalFunction.removeInLineView()
                //                self.tblMatches.reloadData()
            }else {
                APP_DELEGATE.window?.makeToast(responseData.message)
            }
        }
    }
}

extension ChatVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let zCell = tblChat.dequeueReusableCell(withIdentifier: "SendMessageCell", for: indexPath) as! SendMessageCell
        let mCell = tblChat.dequeueReusableCell(withIdentifier: "ReceiveMessageCell", for: indexPath) as! ReceiveMessageCell
        let chatData = chatHistory[indexPath.row]
        
        //        print("message status is \(chatHistory[indexPath.row].author.email == currentAccountInfo.litch.author.email)")
        if chatHistory[indexPath.row].author.email == currentAccountInfo.litch.author.email {
            zCell.lblMessage.text = chatData.text
            return zCell
        }else {
            mCell.lblMessage.text = chatData.text
        }
        return mCell
    }
}

extension ChatVC : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            
            txtMessage.resignFirstResponder()
            print(" you reached end of the table")
        }
    }
    
}
