//
//  ProfileVC.swift
//  Litch
//
//  Created by Kishan Suthar on 01/07/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit
import Alamofire
import MessageUI
import Messages

class ProfileVC: UIViewController,MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblProfileList: UITableView!
    
    var userData : AccountInfoModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblProfileList.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if currentAccountInfo.name != nil {
            lblTitle.text = "Hey \(currentAccountInfo.name ?? ""), here is your current info:"
        }
        
        //        DispatchQueue.main.async {
        //            avPlayer?.pause()
        //            avPlayerLayer?.removeFromSuperlayer()
        //        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.getUserData()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnSendMailAction(_ sender: UIButton) {
        
        if !MFMailComposeViewController.canSendMail() {
            let alertController = UIAlertController(title: "Mail services are not available", message: "Please login into mail account and try again", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
            return
        }else {
            
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            composeVC.setToRecipients(["accounts@kinoke.com"])
            self.present(composeVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnLogoutAction(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Logout", message: "Are you sure want to Logout ?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: { (action) in
            
            UserDefaults.standard.removeObject(forKey: Constant.UserDefaultKeys.currentUserModel)
            
            //            Constant.AppDel.currentLoginUser =
            globalSignUpFinishParam = [:]
            
            //            DispatchQueue.main.async {
            //                avPlayer?.pause()
            //                avPlayerLayer?.removeFromSuperlayer()
            //            }
            
            globalSigupModel = nil
            
            UserDefaults.standard.synchronize()
            //            APP_DELEGATE.setRootVC()
            guard let splashVC = UIStoryboard.init(name: Constant.StoryBoard.main, bundle: nil).instantiateViewController(withIdentifier: "GetStartedVC") as? GetStartedVC else {
                return
            }
            self.navigationController?.pushViewController(splashVC, animated: true)
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK:- like API method call
    private func getUserData() {
        
        WebAPIManager.makeAPIRequest(isFormDataRequest: true,isAuthenticationToken: true, isContainXAPIToken: false, isContainContentType: true, path:Constant.API.getAccountInfo, params: [:]) { (response, status) in
            
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        controller.dismiss(animated: true, completion: nil)
    }
}

extension ProfileVC : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblProfileList.dequeueReusableCell(withIdentifier: "ProfileListTVC", for: indexPath) as! ProfileListTVC
        if indexPath.row == 0 {
            cell.lblTitle.text = "My Litch"
            cell.lblDetail.text = "My Litch"
            cell.btnViewDetail.isHidden = false
            
            if currentAccountInfo?.litch != nil {
                let isoDate = currentAccountInfo?.litch.time ?? ""
                
                if isoDate != "" {
                    let data = convertDateFormatter(date: isoDate)
                    
                    //                print(data)
                    cell.lblDetail.text = "Recorded on \(data)"
                }
            }
            
            
            
            cell.btnViewDetailCompletion = {
                self.performSegue(withIdentifier: Constant.segueIdentifier.segueProfileToMyLitch, sender: self)
            }
            
        }else if indexPath.row == 1 {
            
            if currentAccountInfo?.litch != nil {
                cell.lblTitle.text = "My info"
                cell.lblDetail.text = "\(currentAccountInfo?.name ?? ""), \(currentAccountInfo?.age ?? 0), \(currentAccountInfo?.location ?? "")"
                cell.btnViewDetail.isHidden = true
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func convertDateFormatter(date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let date = dateFormatter.date(from: date)
        
        dateFormatter.dateFormat = "MMM d, yyyy"///this is what you want to convert format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }
}

extension String {
    
    func toDate(withFormat format: String = "yyyy-MM-dd HH:mm:ss")-> Date?{
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)
        return date
    }
}
