//
//  QuestionInstructionVC.swift
//  Litch
//
//  Created by mac on 17/07/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit

var maximumTime = 6

class QuestionInstructionVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: Constant.segueIdentifier.segueInstructionToQueList, sender: self)
        
    }
}
