//
//  GenderInterestVC.swift
//  Litch
//
//  Created by Kishan Suthar on 29/06/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit

class GenderInterestVC: UIViewController {
    
    @IBOutlet weak var btnMyGenderFemale: UIButton!
    @IBOutlet weak var btnMyGenderMale: UIButton!
    
    @IBOutlet weak var btnInterestMale: UIButton!
    @IBOutlet weak var btnInterestFemale: UIButton!
    
    var sex = ""
    var interestSex = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    @IBAction func btnMyGenderMaleAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            self.setMyGender(status: 0)
        }) { (status) in
            
        }
    }
    
    @IBAction func btnMyGenderFemaleAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            self.setMyGender(status: 1)
        }) { (status) in
            
        }
    }
    
    @IBAction func btnInterestMaleAction(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.setInterestGender(status: 0)
        }) { (status) in
            
        }
    }
    
    @IBAction func btnInterestFemaleAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            self.setInterestGender(status: 1)
        }) { (status) in
            
        }
    }
    
    func setMyGender(status: Int) {
        if status == 0 {
            btnMyGenderMale.backgroundColor = #colorLiteral(red: 0.1960784314, green: 0.4941176471, blue: 0.9568627451, alpha: 1)
            btnMyGenderMale.borderColor = #colorLiteral(red: 0.4431372549, green: 0.3333333333, blue: 0.5568627451, alpha: 1)
            btnMyGenderMale.borderWidth = 2
            sex = "Male"
            
            btnMyGenderFemale.backgroundColor = #colorLiteral(red: 0.1764705882, green: 0.1490196078, blue: 0.2431372549, alpha: 1)
            btnMyGenderFemale.borderColor = #colorLiteral(red: 0.4431372549, green: 0.3333333333, blue: 0.5568627451, alpha: 0)
            btnMyGenderFemale.borderWidth = 0
        }else {
            btnMyGenderMale.backgroundColor = #colorLiteral(red: 0.1764705882, green: 0.1490196078, blue: 0.2431372549, alpha: 1)
            btnMyGenderMale.borderColor = #colorLiteral(red: 0.4431372549, green: 0.3333333333, blue: 0.5568627451, alpha: 0)
            btnMyGenderMale.borderWidth = 0
            sex = "Female"
            btnMyGenderFemale.backgroundColor = #colorLiteral(red: 0.1960784314, green: 0.4941176471, blue: 0.9568627451, alpha: 1)
            btnMyGenderFemale.borderColor = #colorLiteral(red: 0.4431372549, green: 0.3333333333, blue: 0.5568627451, alpha: 1)
            btnMyGenderFemale.borderWidth = 2
        }
    }
    
    func setInterestGender(status: Int) {
        if status == 0 {
            btnInterestMale.backgroundColor = #colorLiteral(red: 0.1960784314, green: 0.4941176471, blue: 0.9568627451, alpha: 1)
            btnInterestMale.borderColor = #colorLiteral(red: 0.4431372549, green: 0.3333333333, blue: 0.5568627451, alpha: 1)
            btnInterestMale.borderWidth = 2
            interestSex = "Male"
            btnInterestFemale.backgroundColor = #colorLiteral(red: 0.1764705882, green: 0.1490196078, blue: 0.2431372549, alpha: 1)
            btnInterestFemale.borderColor = #colorLiteral(red: 0.4431372549, green: 0.3333333333, blue: 0.5568627451, alpha: 0)
            btnInterestFemale.borderWidth = 0
        }else {
            btnInterestMale.backgroundColor = #colorLiteral(red: 0.1764705882, green: 0.1490196078, blue: 0.2431372549, alpha: 1)
            btnInterestMale.borderColor = #colorLiteral(red: 0.4431372549, green: 0.3333333333, blue: 0.5568627451, alpha: 0)
            btnInterestMale.borderWidth = 0
            
            interestSex = "Female"
            btnInterestFemale.backgroundColor = #colorLiteral(red: 0.1960784314, green: 0.4941176471, blue: 0.9568627451, alpha: 1)
            btnInterestFemale.borderColor = #colorLiteral(red: 0.4431372549, green: 0.3333333333, blue: 0.5568627451, alpha: 1)
            btnInterestFemale.borderWidth = 2
        }
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        
        if sex == "" {
            self.view.makeToast(Constant.validationMessage.selectYourGender)
        }else if interestSex == "" {
            self.view.makeToast(Constant.validationMessage.selectInterestGender)
        }else {
            globalSignUpFinishParam[Constant.params.sex] = sex
            globalSignUpFinishParam[Constant.params.interestedIn] = interestSex
            self.performSegue(withIdentifier: Constant.segueIdentifier.segueGenderToNameAgeInfo, sender: self)
        }
    }
    
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
