//
//  VideoRecordingVC.swift
//  Litch
//
//  Created by Kishan Suthar on 25/06/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class VideoRecordingVC: FilterCamViewController {
    
    @IBOutlet weak var lblRemainingTime: UILabel!
    @IBOutlet weak var captureButton: UIButton!
    
    var timer: Timer?
    var videoUrl: URL!
    
    override func viewDidLoad() {
        devicePosition = .front
        super.viewDidLoad()
        
        videoQuality = .medium
        shouldShowDebugLabels = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
  
        if APP_DELEGATE.currentLoginUser?.maximumLitchTime == "12" {
            maximumTime = 12
        }else {
            maximumTime = 6
        }
        
        timer?.invalidate()
        captureButton.isSelected = false
        lblRemainingTime.text = "\(maximumTime) second remaining"
        cameraDelegate = self
        
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            
            AVCaptureDevice.requestAccess(for: .audio, completionHandler: { (granted: Bool) in
                if granted {
                    self.cameraDelegate = self
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.performSegue(withIdentifier: Constant.segueIdentifier.segueVideoToPermission, sender: self)
                    }
                }
            })
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    
                    AVCaptureDevice.requestAccess(for: .audio, completionHandler: { (granted: Bool) in
                        if granted {
                            self.cameraDelegate = self
                        } else {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                self.performSegue(withIdentifier: Constant.segueIdentifier.segueVideoToPermission, sender: self)
                            }
                        }
                    })
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.performSegue(withIdentifier: Constant.segueIdentifier.segueVideoToPermission, sender: self)
                    }
                }
            })
        }
    }
    
    func presentCameraSettings() {
        let alertController = UIAlertController(title: "Access denied",
                                      message: "You can enable access to camera in privacy settings",
                                      preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Close", style: .default))
        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        })

        present(alertController, animated: true)
    }
    
    @IBAction func btnCancelAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCaptureVideoAction(_ sender: UIButton) {
        
        UIView.transition(with: captureButton,
                          duration: 1.0,
                          options: .transitionCrossDissolve,
                          animations: {
                            if sender.isSelected {
                                sender.isSelected = false
                                self.stopRecording()
                                self.timer?.invalidate()
                            }else {
                                sender.isSelected = true
                                self.startRecording()
                                self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateAudioRecorderPlay), userInfo: nil, repeats: true)
                            }
        }, completion: nil)
    }
    
    // MARK:- update audio recorder play method
    @objc func updateAudioRecorderPlay()
    {
        maximumTime = maximumTime - 1
        
        if maximumTime == 0 {
            timer?.invalidate()
            lblRemainingTime.text = "\(maximumTime) second remaining"
            stopRecording()
        }else {
            lblRemainingTime.text = "\(maximumTime) second remaining"
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueRecordingToPlayer" {
            let vc = segue.destination as! VideoPlayerVC
            vc.playerUrl = videoUrl
        }
    }
}

extension VideoRecordingVC: FilterCamViewControllerDelegate {
    func filterCamDidStartRecording(_: FilterCamViewController) {}
    
    func filterCamDidFinishRecording(_: FilterCamViewController) {
        print("filterCamDidFinishRecording")
    }
    
    func filterCam(_: FilterCamViewController, didFinishWriting outputURL: URL) {
        
        DispatchQueue.main.async {
            self.videoUrl = outputURL
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
            vc.playerUrl = self.videoUrl
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func filterCam(_: FilterCamViewController, didFocusAtPoint _: CGPoint) {
        print("didFocusAtPoint")
    }
    
    func filterCam(_: FilterCamViewController, didFailToRecord _: Error) {
        print("didFailToRecord")
    }
}
