//
//  QuestionVC.swift
//  Litch
//
//  Created by mac on 17/07/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit
import Alamofire

var globalSignUpFinishParam : Parameters = [:]

class QuestionVC: UIViewController {
    
    @IBOutlet weak var btnWantMoreSecond: UIButton!
    
    var quesArr = [String]()
    
    @IBOutlet weak var cvQuestion: UICollectionView!
    
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    var selectedQuestion = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        selectedQuestion = 0
        
        if globalSigupModel != nil {
            quesArr = globalSigupModel.questions
        }else {
            if quesArr.count == 0 {
                quesArr = Constant.AppDel.currentLoginUser?.questions as! [String]
            }
        }
        
        if APP_DELEGATE.currentLoginUser?.maximumLitchTime == "12" {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.btnWantMoreSecond.isHidden = true
            }
        }else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.btnWantMoreSecond.isHidden = false
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.btnPrevious.isHidden = true
            self.cvQuestion.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constant.segueIdentifier.segueQueListToPopup {
            let vc = segue.destination as! PopupVC
            vc.litchDoubleTimeCompletion = { (status) in 
                maximumTime = 12
                
                var dictionary : [String: Any] = [:]
                dictionary["maximumLitchTime"]  = "12"
                
                APP_DELEGATE.currentLoginUser?.maximumLitchTime = "12"
                APP_DELEGATE.currentLoginUser?.saveInUserDefault()
                
                //
                //                let responseData = LoginModel(fromDictionary: dictionary)
                //                responseData.saveInUserDefault()
                //                Constant.AppDel.currentLoginUser = responseData
                
                if maximumTime == 12 {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.btnWantMoreSecond.isHidden = true
                    }
                }
            }
        }
    }
    
    @IBAction func btnPopupAction(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: Constant.segueIdentifier.segueQueListToPopup, sender: self)
    }
    
    @IBAction func btnPreviousAction(_ sender: UIButton) {
        
        selectedQuestion = selectedQuestion - 1
        self.cvQuestion.scrollToItem(at: IndexPath(item: selectedQuestion, section: 0), at: .right, animated: true)
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        selectedQuestion = selectedQuestion + 1
        self.cvQuestion.scrollToItem(at: IndexPath(item: selectedQuestion, section: 0), at: .right, animated: true)
    }
    
    @IBAction func btnAnswerAction(_ sender: UIButton) {
        
        globalSignUpFinishParam[Constant.params.question] = quesArr[selectedQuestion]
        
        if maximumTime != 12 {
            maximumTime = 6
        }
        
        self.performSegue(withIdentifier: Constant.segueIdentifier.segueQueListToVideoRecording, sender: self)
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension QuestionVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return quesArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = cvQuestion.dequeueReusableCell(withReuseIdentifier: "QuestionList", for: indexPath) as! QuestionList
        cell.lblQuestionName.text = quesArr[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cvQuestion.bounds.width, height: cvQuestion.bounds.width)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        if let ip = cvQuestion.indexPathForItem(at: center) {
            
            if ip.row == 0 {
                
                UIView.transition(with: btnPrevious, duration: 0.4,
                                  options: .transitionCrossDissolve,
                                  animations: {
                                    self.btnPrevious.isHidden = true
                                    self.btnNext.isHidden = false
                })
                selectedQuestion = 0
            }else if ip.row == 1 {
                
                UIView.transition(with: btnNext, duration: 0.4,
                                  options: .transitionCrossDissolve,
                                  animations: {
                                    self.btnNext.isHidden = false
                                    self.btnPrevious.isHidden = false
                })
                selectedQuestion = 1
            }else {
                UIView.transition(with: btnNext, duration: 0.4,
                                  options: .transitionCrossDissolve,
                                  animations: {
                                    self.btnNext.isHidden = true
                                    self.btnPrevious.isHidden = false
                })
                selectedQuestion = 2
            }
        }
    }
}
