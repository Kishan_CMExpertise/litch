//
//  LoginVC.swift
//  Litch
//
//  Created by Kishan Suthar on 29/06/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit
import Alamofire

class LoginVC: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        
        GlobalFunction.showLoadingIndicator(color: 1)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.loginAPICall()
        }
    }
    
    // MARK:- validation for textfield method call
    func validate() -> Bool {
        
        if txtEmail.text == "" {
            self.view.makeToast(Constant.validationMessage.isEmailEmpty)
            return false
        }
        if !GlobalFunction.isValidEmail(email: txtEmail.text ?? "") {
            self.view.makeToast(Constant.validationMessage.isEmailInvalid)
            return false
        }
        if txtPassword.text == "" {
            self.view.makeToast(Constant.validationMessage.isValidPassword)
            return false
        }
        if (txtPassword.text?.count)! < 6  {
            self.view.makeToast(Constant.validationMessage.isValidPasswordRangeTo6)
            return false
        }
        return true
    }
    
    @IBAction func btnForgotPasswordAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: Constant.segueIdentifier.segueLoginToForgotPassword, sender: self)
    }
    
    // MARK:- login API method call
    private func loginAPICall() {
        var params: Parameters = [:]
        params[Constant.params.email]  = txtEmail.text!
        params[Constant.params.password]  = txtPassword.text!
        
        WebAPIManager.makeAPIRequest(isFormDataRequest: true, isContainXAPIToken: false, isContainContentType: true, path:Constant.API.login, params: params) { (response, status) in
            
            let responseData = LoginModel(fromDictionary: response as! [String : Any])
            
            if responseData.status == 200 {
                APP_DELEGATE.window?.makeToast(responseData.message)
                
                responseData.saveInUserDefault()
                Constant.AppDel.currentLoginUser = responseData
                
                self.performSegue(withIdentifier: Constant.segueIdentifier.segueLoginToTab, sender: self)
            }else {
                self.view.makeToast(responseData.message)
            }
        }
    }
}

// MARK:- VCTextFieldDelegate method call
extension LoginVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtEmail {
            txtPassword.becomeFirstResponder()
            return true
        }else if textField == txtPassword {
            txtPassword.resignFirstResponder()
            return true
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == " " {
            return false
        }
        return true
    }
}
