//
//  ForgotPasswordVC.swift
//  Litch
//
//  Created by mac on 17/08/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit
import Alamofire

class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    // MARK:- validation for textfield method call
    func validate() -> Bool {
        
        if txtEmail.text == "" {
            self.view.makeToast(Constant.validationMessage.isEmailEmpty)
            return false
        }
        if !GlobalFunction.isValidEmail(email: txtEmail.text ?? "") {
            self.view.makeToast(Constant.validationMessage.isEmailInvalid)
            return false
        }
        return true
    }
    
    @IBAction func btnResetEmailAction(_ sender: UIButton) {
        
        if validate() {
            GlobalFunction.showLoadingIndicator(color: 1)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.resetEmail()
            }
        }
    }
    
    
    @IBAction func btnGoBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    // MARK:- like API method call
    private func resetEmail() {
        var params: Parameters = [:]
        params[Constant.params.email]  = txtEmail.text
        
        WebAPIManager.makeAPIRequest(isFormDataRequest: true,isAuthenticationToken: false, isContainXAPIToken: false, isContainContentType: true, path:Constant.API.reset, params: params) { (response, status) in
            
            GlobalFunction.hideLoadingIndicator()
            let responseData =  response as! [String : Any]
            
            let status = responseData["status"] as! Int
            let message = responseData["message"] as! String
            
            if status == 200 {
                UIApplication.shared.windows.first?.makeToast(message)
                self.navigationController?.popViewController(animated: true)
            }else {
                UIApplication.shared.windows.first?.makeToast(message)
            }
        }
    }
}

extension ForgotPasswordVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField ==  txtEmail {
            txtEmail.resignFirstResponder()
            return true
        }
        return true
    }
    
}
