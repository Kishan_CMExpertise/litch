//
//  SignUpInVC.swift
//  Litch
//
//  Created by Kishan Suthar on 26/06/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit

class SignUpInVC: UIViewController {
    
    @IBOutlet weak var loginBorderView: UIView!
    @IBOutlet weak var signUpBorderView: UIView!
    
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setUpTab(status: 0)
    }
    
    @IBAction func btnSelectSignUpAction(_ sender: UIButton) {
        setUpTab(status: 0)
    }
    
    @IBAction func btnSelectLoginAction(_ sender: UIButton) {
        setUpTab(status: 1)
    }
    
    func setUpTab(status: Int) {
        if status  == 0 {
            UIView.animate(withDuration: 0.3, animations: {
                self.signUpBorderView.isHidden = false
                self.loginBorderView.isHidden = true
            }) { (status) in
            }
            setContainerVC(status: 0)
        }else {
            UIView.animate(withDuration: 0.3, animations: {
                self.signUpBorderView.isHidden = true
                self.loginBorderView.isHidden = false
            }) { (status) in
            }
            setContainerVC(status: 1)
        }
    }
    
    func setContainerVC(status : Int) {
        if status == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
            addChild(vc)
            vc.view.frame = CGRect.init(x: 0, y: 0, width: self.containerView.frame.size.width, height: self.containerView.frame.size.height)
            self.containerView.addSubview((vc.view)!)
            vc.didMove(toParent: self)
        }else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            addChild(vc)
            vc.view.frame = CGRect.init(x: 0, y: 0, width: self.containerView.frame.size.width, height: self.containerView.frame.size.height)
            self.containerView.addSubview((vc.view)!)
            vc.didMove(toParent: self)
        }
    }
}
