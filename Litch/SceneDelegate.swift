//
//  SceneDelegate.swift
//  Litch
//
//  Created by Kishan Suthar on 25/06/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var currentLoginUser : LoginModel?

    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let _ = (scene as? UIWindowScene) else { return }
        StoreManager.shared.updateProducts()
        setRootVC()
    }
    
    func checkForUserLogin() -> Bool {
        guard let decoded  = Constant.UserDefault.object(forKey: Constant.UserDefaultKeys.currentUserModel) as? Data else { return false }
        guard let decodedUser = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? LoginModel else { return false }
        if decodedUser.token == "" || decodedUser.token == nil || decodedUser.name == "" || decodedUser.name == nil {
            return false
        }
        currentLoginUser = decodedUser
        return true
    }
    
    func setRootVC() {
        
        var navigationController: UINavigationController!
        
        //        navigationController = UINavigationController.init(rootViewController: splashVC)
        
        if checkForUserLogin() {
            guard let homeVC = UIStoryboard.init(name: Constant.StoryBoard.main, bundle: nil).instantiateViewController(withIdentifier: "HomeTabVC") as? HomeTabVC else {
                return
            }
            navigationController = UINavigationController.init(rootViewController: homeVC)
        }else {
            
            guard let decoded  = Constant.UserDefault.object(forKey: Constant.UserDefaultKeys.currentUserModel) as? Data else { return }
            
            let decodedUser = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? LoginModel
            
            if decodedUser?.token != nil {
                guard let questIntro = UIStoryboard.init(name: Constant.StoryBoard.main, bundle: nil).instantiateViewController(withIdentifier: "QuestionVC") as? QuestionVC else {
                    return
                }
                
                navigationController = UINavigationController.init(rootViewController: questIntro)
            }else {
                guard let splashVC = UIStoryboard.init(name: Constant.StoryBoard.main, bundle: nil).instantiateViewController(withIdentifier: "GetStartedVC") as? GetStartedVC else {
                    return
                }
                
                navigationController = UINavigationController.init(rootViewController: splashVC)
            }
        }
        navigationController.isNavigationBarHidden = true
        UIApplication.shared.windows.first?.rootViewController = navigationController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }

    @available(iOS 13.0, *)
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    @available(iOS 13.0, *)
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    @available(iOS 13.0, *)
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    @available(iOS 13.0, *)
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    @available(iOS 13.0, *)
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

