//
//  AppDelegate.swift
//  Litch
//
//  Created by Kishan Suthar on 25/06/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var spinnerView:MMMaterialDesignSpinner?
    var currentLoginUser : LoginModel?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        StoreManager.shared.updateProducts()
        setRootVC()
        return true
    }
    
    func checkForUserLogin() -> Bool {
        guard let decoded  = Constant.UserDefault.object(forKey: Constant.UserDefaultKeys.currentUserModel) as? Data else { return false }
        guard let decodedUser = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? LoginModel else { return false }
        if decodedUser.token == "" || decodedUser.token == nil || decodedUser.name == "" || decodedUser.name == nil {
            return false
        }
        currentLoginUser = decodedUser
        return true
    }
    
    func setRootVC() {
        
        var navigationController: UINavigationController!
        
        //        navigationController = UINavigationController.init(rootViewController: splashVC)
        
        if checkForUserLogin() {
            guard let homeVC = UIStoryboard.init(name: Constant.StoryBoard.main, bundle: nil).instantiateViewController(withIdentifier: "HomeTabVC") as? HomeTabVC else {
                return
            }
            navigationController = UINavigationController.init(rootViewController: homeVC)
        }else {
            
            guard let decoded  = Constant.UserDefault.object(forKey: Constant.UserDefaultKeys.currentUserModel) as? Data else { return }
            
            let decodedUser = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? LoginModel
            
            if decodedUser?.token != nil {
                guard let questIntro = UIStoryboard.init(name: Constant.StoryBoard.main, bundle: nil).instantiateViewController(withIdentifier: "QuestionVC") as? QuestionVC else {
                    return
                }
                currentLoginUser = decodedUser
                
                navigationController = UINavigationController.init(rootViewController: questIntro)
            }else {
                guard let splashVC = UIStoryboard.init(name: Constant.StoryBoard.main, bundle: nil).instantiateViewController(withIdentifier: "GetStartedVC") as? GetStartedVC else {
                    return
                }
                
                navigationController = UINavigationController.init(rootViewController: splashVC)
            }
        }
        navigationController.isNavigationBarHidden = true
        UIApplication.shared.windows.first?.rootViewController = navigationController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

