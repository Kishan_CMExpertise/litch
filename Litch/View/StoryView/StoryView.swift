//
//  StoryView.swift
//  KinokeCardView
//
//  Created by mac on 15/05/19.
//  Copyright © 2019 CMExpertise Infotech pvt. ltd. All rights reserved.
//

import UIKit

class StoryView: UIView {
    
     // MARK:- declare outlet and variables
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imgStory: UIImageView!
    @IBOutlet weak var bgView: UIImageView!
    
    @IBOutlet weak var btnReply: UIButton!
    
    @IBOutlet weak var blurView: UIView!
    var readMoreOnIndex: IndexPath?
    var returnInviteBlock: ((Bool) -> ())?
    var returnCommentBlock: ((Bool) -> ())?
    var returnBackCommentBlock: ((Bool) -> ())?
    
    var returnReplyBlock: (() -> ())?
    var strOffer: String = ""
    
    var name : String = ""
    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        commonInit()
//    }
//
//    init() {
//        super.init(frame: CGRect.zero)
//        commonInit()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        commonInit()
//    }
//
//     // MARK:- common init method call
//    private func commonInit() {
//        Bundle.main.loadNibNamed("StoryView", owner: self, options: nil)
//        contentView.frame = bounds
//        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
//        addSubview(contentView)
//
//    }
    
    @IBAction func btnReplyAction(_ sender: UIButton) {
        returnReplyBlock!()
    }
    
    deinit {
//     print("Storyview deinit successfully")
    }
    
}
