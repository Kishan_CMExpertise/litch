
import Foundation
import UIKit
import AVFoundation
import SDWebImage

class ExampleCardCell: CardCell {
    
    @IBOutlet weak var imgBlur: UIImageView!
    @IBOutlet weak var imgLitch: UIImageView!
    @IBOutlet weak var btnReplay: UIButton!
    
    var videoURL: String?
    
    var btnReplayCompletion: (()->())?
    var btnMoreCompletion: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            self.imgBlur.isHidden = true
            self.btnReplay.isHidden = true
            self.cornerRadius = 20
            self.imgLitch.layer.cornerRadius = 20
        }
        
        self.clipsToBounds = true
        imgLitch.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func configureCell(imageUrl: String?,
                       description: String,
                       videoUrl: String?) {
        
        self.imgLitch.sd_setImage(with: URL(string: imageUrl ?? ""), placeholderImage: #imageLiteral(resourceName: "video_placeholder"), options: .continueInBackground, context: nil)
        self.videoURL = videoUrl
    }
    
    @IBAction func btnReplayAction(_ sender: UIButton) {
        btnReplayCompletion?()
    }
    
    @IBAction func btnMoreAction(_ sender: UIButton) {
        btnMoreCompletion?()
    }
}
