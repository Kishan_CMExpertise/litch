//
//  FavoriteMatchTVC.swift
//  Litch
//
//  Created by Kishan Suthar on 30/06/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit

class FavoriteMatchTVC: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
