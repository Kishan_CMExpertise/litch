//
//  ProfileListTVC.swift
//  Litch
//
//  Created by Kishan Suthar on 01/07/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit

class ProfileListTVC: UITableViewCell {

    @IBOutlet weak var btnViewDetail: UIButton!
    
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    var btnViewDetailCompletion: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func btnViewDetailAction(_ sender: UIButton) {
        btnViewDetailCompletion?()
    }
    
}
