//
//  SendMessageCell.swift
//  Litch
//
//  Created by mac on 27/07/20.
//  Copyright © 2020 Kishan Suthar. All rights reserved.
//

import UIKit

class SendMessageCell: UITableViewCell {

    @IBOutlet weak var lblMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
